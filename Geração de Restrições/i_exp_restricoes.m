function [ I_Exp_restricoes ] = i_exp_restricoes( A_base, I_Exp_restricoes, num_cen, R_Exp_I, N )
%I_EXP_RESTRICOES Retorna o indice das restri��es de expans�o de I
%   Esta fun��o recebe os indices das restri��es de expans�o para os limites 
%   de interc�mbio para o primeiro cen�rio e retorna os indices das restri��es 
%   de expans�o de limites de interc�mbio do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.

% N�mero de restri��es por submercado, tempo e cen�rio
num_restricoes = kron(ones(1,N-1),(kron(size(A_base,1), ones(1,size(R_Exp_I.p1,2))) - R_Exp_I.p1));


% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    I_Exp_restricoes = I_Exp_restricoes(1:size(I_Exp_restricoes,2) - 5) + ...
        kron(ones(1,N-1), I_Exp_restricoes(size(I_Exp_restricoes,2) - 4:end)) + num_restricoes; 
else
    aux_I_Exp_temp = i_exp_restricoes(A_base,I_Exp_restricoes,num_cen - 1,R_Exp_I,N);
    I_Exp_restricoes = I_Exp_restricoes(1:size(I_Exp_restricoes,2) - 5) + ...
        kron(ones(1,N-1), aux_I_Exp_temp(size(aux_I_Exp_temp,2) - 4:end)) + num_restricoes; 
end

end

