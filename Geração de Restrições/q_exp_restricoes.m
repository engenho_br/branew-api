function [ Q_exp_restricoes ] = q_exp_restricoes(A_base, Q_exp_restricoes, num_cen, R_Exp_Q, N, N_submercado )
%Q_EXP_RESTRICOES Retorna o indice das restri��es de expans�o de turbinamento
%   Esta fun��o recebe os indices das restri��es de expans�o de turbinamento 
%   para o primeiro cen�rio e retorna os indices das restri��es 
%   de expans�o de turbinamento do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.

Q_exp_p1 = [];
for i = 1:N_submercado
    Q_exp_p1 = [Q_exp_p1  R_Exp_Q(i).p1];
end

% N�mero de restri��es por submercado, tempo e cen�rio
num_restricoes = kron(ones(1,N-1),(kron(size(A_base,1), ...
    ones(1,size(Q_exp_p1,2))) - Q_exp_p1));

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    Q_exp_restricoes = Q_exp_restricoes(1:size(Q_exp_restricoes,2) - size(Q_exp_p1,2)) + ...
        kron(ones(1,N-1), Q_exp_restricoes(size(Q_exp_restricoes,2)...
        - size(Q_exp_p1,2) + 1:end)) + num_restricoes; 
else
    auxQ_Exp_temp = q_exp_restricoes(A_base, Q_exp_restricoes, num_cen - 1, R_Exp_Q, N,...
        N_submercado);
    Q_exp_restricoes = Q_exp_restricoes(1:size(Q_exp_restricoes,2) - size(Q_exp_p1,2) ) + ...
        kron(ones(1,N-1), auxQ_Exp_temp(size(auxQ_Exp_temp,2) ...
        - size(Q_exp_p1,2) + 1:end)) + num_restricoes;
end

end

