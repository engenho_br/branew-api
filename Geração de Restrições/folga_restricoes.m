function [ F_restricoes ] = folga_restricoes( F_restricoes, num_cen, A_base, RFolga )
%FOLGA_RESTRICOES Retorna o indice das restri��es de folga
%   Esta fun��o recebe os indices das restri��es de folga para o primeiro cen�rio e
%   retorna os indices das restri��es de folga do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.

% Retirando restri��es do primeiro cen�rio, primeiro tempo
size_auxF = size(F_restricoes,2) - 4;

% N�mero de restri��es por submercado, tempo e cen�rio
num_restricoes = size(A_base,1) - RFolga(4).p1;

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen == 2
    F_restricoes = F_restricoes(1:size_auxF) + F_restricoes(end)*ones(1,size_auxF) + num_restricoes;
else
    F_restricoes_end = folga_restricoes(F_restricoes,num_cen -1, A_base, RFolga);
    F_restricoes_end = F_restricoes_end(end);
    F_restricoes = F_restricoes(1:size_auxF) + F_restricoes_end*ones(1,size_auxF) + num_restricoes;
end

end

