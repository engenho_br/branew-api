function [ A_ineq, B_ineq, indices_excluidos_A, indices_excluidos_B, R, auxR ] = ...
    geracao_restricao_inequacao( A_sparse, B, num_cenario, N, I, contR, RFolga, R_Exp_Q,...
    Ab, N_submercado, R_Exp_V, R_Exp_I_pos, R_Exp_I_neg, R_Exp_I_T, R_Exp_T,...
     R_Exp_T_T, R_Exp_GH_T)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
A_ineq = [];
B_ineq = [];
indices_excluidos_A = [];
indices_excluidos_B = [];

for cen = 1:num_cenario
    
    % Pegando ind?ces das inequa??es para o cen?rio 1
    R = contR.*([1:N]-1);
    auxR = kron(R,ones(1,N_submercado));
    aux_restricoes_I = kron(R,ones(1,size(I.linhas,1)));
    
    % Restri??o de Folga
    F_restricoes = kron(ones(1,length(R)),[RFolga(:).p1]) + auxR;
    
    % Pegando a quantidade de constraints de Q_exp, p_GH_exp e V_exp  
    % por cen�rio e tempo.
    length_Q_exp = 0;
    length_GH_exp_T = 0;
    length_V_exp = 0;
    
    for sub = 1:N_submercado
        length_Q_exp = length_Q_exp + length(R_Exp_Q(sub).p1);
        length_GH_exp_T = length_GH_exp_T + length(R_Exp_GH_T(sub).p1);
        length_V_exp = length_V_exp + length(R_Exp_V(sub).p1);
    end
    
    % Restri��o de expans�o de Turbinamento
    Q_restricoes = kron(ones(1,N),[R_Exp_Q(:).p1]) + kron(R,ones(1,length_Q_exp));
    
    GH_T_restricoes =kron(ones(1,N),[R_Exp_GH_T(:).p1]) + kron(R,ones(1,length_GH_exp_T));
    
    % Restri��o de expans�o de Armazenamento
    V_restricoes = kron(ones(1,N),[R_Exp_V(:).p1]) + kron(R,ones(1,length_V_exp));
    
    
    % Restri��es de expans�o do Limite de Interc�mbio
    I_restricoes_pos = kron(ones(1,N),[R_Exp_I_pos.p1]) + aux_restricoes_I;
    I_restricoes_neg = kron(ones(1,N),[R_Exp_I_neg.p1]) + aux_restricoes_I;
    I_restricoes_T = kron(ones(1,N),[R_Exp_I_T.p1]) + aux_restricoes_I;
    
    % Restri��es de expans�o de T�rmicas
    
    length_T_exp = 0;
    length_T_exp_T = 0;
    for sub = 1:N_submercado
        length_T_exp = length_T_exp + length(R_Exp_T(sub).p1);
        length_T_exp_T = length_T_exp_T + length(R_Exp_T_T(sub).p1);
    end
    
    T_T_restricoes = kron(ones(1,N),[R_Exp_T_T(:).p1]) + kron(R,ones(1,length_T_exp_T));
    T_restricoes = kron(ones(1,N),[R_Exp_T(:).p1]) + kron(R,ones(1,length_T_exp));
    
    % Se o cen?rio for diferente de 1, calcule o novo auxF para este cen?rio
    if cen>1        
        F_restricoes = folga_restricoes(F_restricoes, cen, Ab, RFolga);
        Q_restricoes = q_exp_restricoes(Ab, Q_restricoes, cen, R_Exp_Q, ...
            N, N_submercado);
        V_restricoes = v_exp_restricoes(Ab, V_restricoes, cen, R_Exp_V,...
            N, N_submercado);
        GH_T_restricoes = q_exp_restricoes(Ab, GH_T_restricoes, cen, R_Exp_GH_T,...
            N, N_submercado);
        
        I_restricoes_pos = i_exp_restricoes(Ab, I_restricoes_pos, cen, R_Exp_I_pos, N);
        I_restricoes_neg = i_exp_restricoes(Ab, I_restricoes_neg, cen, R_Exp_I_neg, N);
        I_restricoes_T = i_exp_restricoes(Ab, I_restricoes_T, cen, R_Exp_I_T, N);
        
        T_restricoes = t_exp_restricoes( Ab, T_restricoes, cen, R_Exp_T, N,...
            N_submercado);
        T_T_restricoes = t_exp_restricoes( Ab, T_T_restricoes, cen, R_Exp_T_T, N,...
            N_submercado);
    end    
    
    

    
    % Armazene os indices das inequa??es para poder exclui-las depois de
    % criar a matriz de inequa??o

    indices_excluidos_A = [indices_excluidos_A F_restricoes Q_restricoes  ...
        V_restricoes GH_T_restricoes I_restricoes_pos I_restricoes_neg ...
        I_restricoes_T T_restricoes T_T_restricoes];
    indices_excluidos_B = [indices_excluidos_B F_restricoes Q_restricoes ...
        V_restricoes GH_T_restricoes I_restricoes_pos I_restricoes_neg ...
        I_restricoes_T T_restricoes T_T_restricoes];


    % Adicione as inequa??es deste cen?rio na matriz de inequa??es

    inequacoes = [-1*A_sparse(F_restricoes, :); A_sparse(Q_restricoes,:); ...
        A_sparse(V_restricoes,:); A_sparse(GH_T_restricoes,:); ...
        A_sparse(I_restricoes_pos,:); A_sparse(I_restricoes_neg,:); A_sparse(I_restricoes_T,:);...
        A_sparse(T_restricoes,:); A_sparse(T_T_restricoes,:);];
    valor_inequacoes = [-1*B(F_restricoes); B(Q_restricoes); B(V_restricoes); ...
       B(GH_T_restricoes); B(I_restricoes_pos); B(I_restricoes_neg);...
       B(I_restricoes_T); B(T_restricoes); B(T_T_restricoes);];
    A_ineq = [A_ineq; inequacoes ];
    B_ineq = [B_ineq; valor_inequacoes];

end
A_sparse(indices_excluidos_A,:) = [];
B(indices_excluidos_B) = [];

end