function [ T_exp_restricoes ] = t_exp_restricoes( A_base, T_exp_restricoes, num_cen, R_Exp_Termica, N,...
    N_submercado)
%T_EXP_RESTRICOES Retorna o indice das restri��es de expans�o de t�rmicas
%   Esta fun��o recebe os indices das restri��es de expans�o de t�rmicas 
%   para o primeiro cen�rio e retorna os indices das restri��es 
%   de expans�o de t�rmicas do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.


% Pegando os indices das t�rmicas do primeiro tempo e cen�rio
T_exp_p1 = [];
for i = 1:N_submercado
    T_exp_p1 = [T_exp_p1  R_Exp_Termica(i).p1];
end

% N�mero de restri��es por submercado, tempo e cen�rio
num_restricoes = kron(ones(1,N-1),(kron(size(A_base,1), ...
    ones(1,size(T_exp_p1,2))) - T_exp_p1));

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    T_exp_restricoes = T_exp_restricoes(1:size(T_exp_restricoes,2) - size(T_exp_p1,2)) + ...
        kron(ones(1,N-1), T_exp_restricoes(size(T_exp_restricoes,2)...
        - size(T_exp_p1,2) + 1:end)) + num_restricoes; 
else
    auxT_Exp_temp = t_exp_restricoes(A_base, T_exp_restricoes, num_cen - 1, R_Exp_Termica, N,...
        N_submercado);
    T_exp_restricoes = T_exp_restricoes(1:size(T_exp_restricoes,2) - size(T_exp_p1,2) ) + ...
        kron(ones(1,N-1), auxT_Exp_temp(size(auxT_Exp_temp,2) ...
        - size(T_exp_p1,2) + 1:end)) + num_restricoes; 
    
end

end

