function [ V_exp_restricoes ] = v_exp_restricoes(A_base, V_exp_restricoes,num_cen, R_Exp_V, N, N_submercado )
%V_EXP_RESTRICOES Retorna o indice das restri��es de expans�o de armazenamento
%   Esta fun��o recebe os indices das restri��es de expans�o de armazenamento 
%   para o primeiro cen�rio e retorna os indices das restri��es 
%   de expans�o de armazenamento do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.

V_exp_p1 = [];
for i = 1:N_submercado
    V_exp_p1 = [V_exp_p1  R_Exp_V(i).p1];
end

% N�mero de restri��es por submercado, tempo e cen�rio
num_restricoes = kron(ones(1,N-1),(kron(size(A_base,1), ...
    ones(1,size(V_exp_p1,2))) - V_exp_p1));

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    V_exp_restricoes = V_exp_restricoes(1:size(V_exp_restricoes,2) - size(V_exp_p1,2)) + ...
        kron(ones(1,N-1), V_exp_restricoes(size(V_exp_restricoes,2)...
        - size(V_exp_p1,2) + 1:end)) + num_restricoes; 
else
    auxV_Exp_temp = v_exp_restricoes(A_base, V_exp_restricoes, num_cen - 1, R_Exp_V, N,...
        N_submercado);
    V_exp_restricoes = V_exp_restricoes(1:size(V_exp_restricoes,2) - size(V_exp_p1,2) ) + ...
        kron(ones(1,N-1), auxV_Exp_temp(size(auxV_Exp_temp,2) ...
        - size(V_exp_p1,2) + 1:end)) + num_restricoes;
end
end

