function [ inflex, termicas, term_def , ger_liq, engfio, Fe, maiores_i, tot_ter ] = operacao_submercado( ii, F, resp, T, handles )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


inflex=sum(T(ii).minimo,2);
    engfio=handles.EF(ii,:,cenario)';
    %% 'F1I', 'F2I', 'FI3', 'F23', 'F34'
    % Pegando os fluxos de cada subsistema, fluxo que entra e fluxo que sai
    switch ii
        case 1 % NORTE
            fluxos=F(cenario).F(:,1);
            I=find(fluxos<0);
            Fe=zeros(size(fluxos));
            Fe(I)=-fluxos(I);
            Fe=sum(Fe,2);
            fluxos(I)=0;
            ger_liq=resp(ii).Q(:,cenario)-fluxos-engfio;
            Q=resp(ii).Q(:,cenario);
        case 2 % NORDESTE
            fluxos=F(cenario).F(:,[2 4]);
            I=find(fluxos<0);
            Fe=zeros(size(fluxos));
            Fe(I)=fluxos(I);
            Fe=-sum(Fe,2);
            fluxos(I)=0;
            ger_liq=resp(ii).Q(:,cenario)-sum(fluxos,2)-engfio;
            Q=resp(ii).Q(:,cenario);
        case 3 % SUDESTE
            fluxos=F(cenario).F(:,3:4);
            I=find(fluxos(:,1:2)>0);
            Fe=zeros(size(fluxos));
            Fe(I)=fluxos(I);
            Fe=sum(Fe,2);
            fluxos(I)=0;
            ger_liq=resp(ii).Q(:,cenario)+sum(fluxos(:,1:2),2);

            fluxos=F(cenario).F(:,5);
            I=find(fluxos(:,1)<0);
            aux=zeros(size(fluxos));
            aux(I)=fluxos(I);
            Fe=Fe-aux;
            fluxos(I)=0;
            ger_liq=ger_liq-fluxos(:,1)-engfio;
            Q=resp(ii).Q(:,cenario);
        case 4 % SUL
            fluxos=F(cenario).F(:,5);
            I=find(fluxos(:,1)>0);
            Fe=zeros(size(fluxos));
            Fe(I)=fluxos(I);
            Fe=sum(Fe,2);
            fluxos(I)=0;
            ger_liq=resp(ii).Q(:,cenario)+fluxos(:,1)-engfio;
            Q=resp(ii).Q(:,cenario);
    end

    % Produ��o e custo de t�rmicas

    termicas=T(ii).maximo(:,1:end-1)-T(ii).minimo(:,1:end-1);
    custo=T(ii).custo(:,1:end-1);
    
    % Ordenar por custo
    if size(custo,2)~=1
        for iii=1:size(termicas,1)
            [lixo,I]=sort(custo(iii,:));
            termicas(iii,:)=termicas(iii,I);
        end
    end
    
    
    maiores_i=[];
    for iii=1:3
        aux_i=find(I==max(I)-iii+1);
        maiores_i=[maiores_i; aux_i];
    end

    term_def=resp(ii).T(:,end,cenario);
    tot_ter=sum(resp(ii).T(:,:,cenario)',1)';
    I=find(ger_liq<0);
    term_def(I)=term_def(I)+ger_liq(I);
    ger_liq(I)=0;


end

