function y = nansum(x,varargin)
%NANSUM Sum ignoring NaNs.
%   NANSUM(X) returns the sum treating NaNs as missing values.  
%   For vectors, NANSUM(X) is the sum of the non-NaN elements in
%   X. For matrices, NANSUM(X) is a row vector containing the sum 
%   of the non-NaN elements in each column of X. 
%   Ks, 02.08.03: extension to any dimension.
%
%    See also NANMEDIAN, NANSTD, NANMIN, NANMAX, NANMEAN.

%   Copyright 1993-2002 The MathWorks, Inc. 
%   $Revision: 2.10 $  $Date: 2002/01/17 21:31:14 $

if nargin==1                           % original function
% Replace NaNs with zeros.
nans = isnan(x);
i = find(nans);
x(i) = zeros(size(i));

% Protect against an entire column of NaNs
y = sum(x);
i = find(all(nans));
y(i) = i + NaN;

elseif nargin==2 & any(size(varargin{1})==1)   % mean over a given dimension

dims=varargin{1};

dims=fliplr(reshape(sort(dims),[1 length(dims)]));

% Replace NaNs with zeros.
nans = ~isnan(x);
nulls = (x==0);
x(find(~nans)) = 0;

% Protect against an entire column of NaNs
for dim=dims
  x = sum(x,dim);
  nans=sum(nans,dim);
  nulls=sum(nulls,dim);
end

nans(find(~nans & ~nulls))=NaN;    % columns with only NaN return to NaN.
y=squeeze(x.*sign(nans));

else

error(['Wrong imput : ',num2str(varargin{:})])

end