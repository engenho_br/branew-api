function [ N, v_inicial, tempo, prob_cenario, tx_desc, mes_inicial, ano_inicial, n_anos ] ...
    = le_dados_gerais(a, b, indice,handles)
%LE_DADOS_GERAIS Leitura dos dados gerais de um deck newave
%   Faz a leitura dos dados gerais de um deck newave para que possa saber o
%   per�odo de tempo que o deck cobre e pegar informa��es tais como volume
%   inicial, probabilidade de ocorr�ncia de cada cen�rio e taxa de desconto

%%% In�cio e Dura��o do Estudo
mes_inicial  = a{1}(5);
ano_inicial  = a{1}(6);
n_anos = 0;

% num_meses = str2num(get(handles.edit_N_meses,'String'));
num_meses = handles.edit_N_meses;
if num_meses > 12
    n_anos = floor(num_meses/12);
    mes_final = num_meses - n_anos*12 + mes_inicial - 1;
else
    mes_final = num_meses - n_anos*12 + mes_inicial - 1;
end

tempo = datevec([datenum(ano_inicial,mes_inicial,1): datenum(ano_inicial+n_anos,mes_final,1)]);
tempo(:,3) = 1;
tempo = unique(datenum(tempo));

N = length(tempo);

%%% Volumes Iniciais
v_inicial = a{1}(22:25,1);
v_inicial(indice) = v_inicial;

%%% Probabilidades (mais a frente ser� feito um check com o n�mero de cen�rios)
II = strmatch('Probabilidades',b{1});
prob_cenario = a{1}(II-1,:)/nansum(a{1}(II-1,:));

%%% Taxa de Descontos
tx_desc = a{1}(27,1);

end

