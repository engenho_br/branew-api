function [ p_T_exp, T_exp, p_cons_T_exp] = le_termicas_de_expansao( a, b, indice, N_submercado, tempo, n_anos )
  %LE_TERMICAS Leitura dos dados de t�rmica do deck do newave
  %   Faz a leitura dos dados das t�rmicas do excel do deck do newave,
  %   organiza-os por submercado, separando por custo, c�digo, nome, m�ximo,
  %   m�nimo e n�mero da t�rmica.
  p_T_exp = [];
  texto = b{1,30};
  dados_configuracao = a{1,30};        %%% C�digo e submercado das t�rmicas
  dados_termo  = a{1,31}; %%% C�digo, POT, FCMAX, TEIF, IP e GTMIN
  dados_custo  = a{1,32}; %%% C�digo e custos anuais
  % dados_custo1 = a{1,9}; %%% C�digo e custo do m�s 1
  texto_custo = b{1,32};

  data_inicial = datevec(tempo(1));

  if ~isempty(dados_configuracao)
    legenda = [dados_configuracao(:,1) dados_configuracao(:,3)];
  else
    legenda = [];
  end

  for ii=1:N_submercado

    if ~isempty(dados_configuracao)
      indice_termicas_sub = find(dados_configuracao(:,3)==ii);
      T_exp(ii).codigo = dados_configuracao(indice_termicas_sub,5);
      T_exp(ii).nome   = char(texto(indice_termicas_sub+1,2));
      tempo_construcao = dados_configuracao(indice_termicas_sub,7);
    else
      indice_termicas_sub = [];
      T_exp(ii).codigo = zeros(length(tempo),0);
      T_exp(ii).codigo = zeros(length(tempo),0);
      tempo_construcao = zeros(1,0);
    end

    T_exp(ii).numero = size(indice_termicas_sub,1);
    T_exp(ii).maximo = zeros(length(tempo),T_exp(ii).numero);
    T_exp(ii).minimo = zeros(length(tempo),T_exp(ii).numero);
    T_exp(ii).custo  = zeros(length(tempo),T_exp(ii).numero);
    T_exp(ii).fonte  = [];
    p_T_exp(ii).minimo = zeros(length(tempo),T_exp(ii).numero);
    p_T_exp(ii).maximo = Inf*ones(length(tempo),T_exp(ii).numero);
    p_T_exp(ii).custo = zeros(length(tempo),T_exp(ii).numero);
    p_cons_T_exp(ii).minimo = zeros(length(tempo),T_exp(ii).numero);
    p_cons_T_exp(ii).maximo = Inf*ones(length(tempo),T_exp(ii).numero);
    p_cons_T_exp(ii).custo = zeros(length(tempo),T_exp(ii).numero);


    for iii = 1:T_exp(ii).numero
      indice_termica = find(dados_termo(:,1)==T_exp(ii).codigo(iii));

      p_cons_T_exp(ii).custo(:,iii) = ...
      dados_configuracao(indice_termica,6)*ones(length(tempo),1);
    
      p_T_exp(ii).maximo(1:tempo_construcao(iii),iii) = 0;

      T_exp(ii).maximo(:,iii) = dados_termo(indice_termica,3) * ...
      (dados_termo(indice_termica,4)/100) * ...
      (1-(dados_termo(indice_termica,5)/100)) * ...
      (1-(dados_termo(indice_termica,6)/100));
      %              POT       *
      %             (FCMX/100)       *
      %             (1 - (TEIF/100))      *
      %             (1 - (IP/100))

      indice_mes_final = 18;
      if length(tempo) + data_inicial(2) < 14
        indice_mes_final = 6 + data_inicial(2) + length(tempo) - 1;
      end

      T_exp(ii).minimo(:,iii) = [dados_termo(indice_termica,6+data_inicial(2):indice_mes_final)';...  % Minimo do primeiro ano
      ones(size(tempo,1)-13+data_inicial(2),1)*dados_termo(indice_termica,19)]; % Minimo do demais anos


      indice_termica = find(dados_custo(:,1)==T_exp(ii).codigo(iii));

      switch n_anos
        case 0
          T_exp(ii).custo(:,iii) = [ ... % Custo do primeiro ano
          ones(length(tempo),1)*dados_custo(indice_termica,3);...
          ];

        case 1
          T_exp(ii).custo(:,iii) = [ ...
          ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
          ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
          ];
        case 2
          T_exp(ii).custo(:,iii) = [ ...
          ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
          ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
          ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
          ];
        case 3
          T_exp(ii).custo(:,iii) = [ ...
          ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
          ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
          ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
          ones(12,1)*dados_custo(indice_termica,6);...            % Custo do quarto ano
          ];
        otherwise
          T_exp(ii).custo(:,iii) = [ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
          ones(12,1)*dados_custo(indice_termica,4);...            % Custo do segundo ano
          ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
          ones(12,1)*dados_custo(indice_termica,6);...            % Custo do quarto ano
          ones(12,1)*dados_custo(indice_termica,7)];              % Custo do quinto ano

      end


      T_exp(ii).fonte = [ T_exp(ii).fonte texto_custo(indice_termica,8)];

      %         if ~isempty(dados_custo1)
      %             indice_termica = find(dados_custo1(:,1)==T_exp(ii).codigo(iii));
      % %             if ~isempty(indice_termica)
      % %                 T(ii).custo(1,iii) = dados_custo1(indice_termica,3);
      % %             end
      %         end
    end

  end

  T_exp(indice) = T_exp;
  p_T_exp(indice) = p_T_exp;
  p_cons_T_exp(indice) = p_cons_T_exp;

end
