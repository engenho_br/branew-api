function [ T, legenda, dados_termo ] = le_termicas( a, b, indice, N_submercado, tempo, n_anos )
%LE_TERMICAS Leitura dos dados de t�rmica do deck do newave
%   Faz a leitura dos dados das t�rmicas do excel do deck do newave,
%   organiza-os por submercado, separando por custo, c�digo, nome, m�ximo,
%   m�nimo e n�mero da t�rmica.

texto = b{1,6};
dados_configuracao = a{1,6};        %%% C�digo e submercado das t�rmicas
dados_termo  = a{1,7}; %%% C�digo, POT, FCMAX, TEIF, IP e GTMIN
dados_custo  = a{1,8}; %%% C�digo e custos anuais
dados_custo1 = a{1,9}; %%% C�digo e custo do m�s 1
texto_custo = b{1,8};

data_inicial = datevec(tempo(1));

legenda = [dados_configuracao(:,1) dados_configuracao(:,3)];
for ii=1:N_submercado
    indice_termicas_sub = find(dados_configuracao(:,3)==ii);
    
    T(ii).codigo = dados_configuracao(indice_termicas_sub,5);
    T(ii).nome   = char(texto(indice_termicas_sub+1,2));
    T(ii).numero = size(indice_termicas_sub,1);
    T(ii).maximo = zeros(length(tempo),T(ii).numero);
    T(ii).minimo = zeros(length(tempo),T(ii).numero);
    T(ii).custo  = zeros(length(tempo),T(ii).numero);
    T(ii).fonte  = [];
    
    for iii = 1:T(ii).numero
        indice_termica = find(dados_termo(:,1)==T(ii).codigo(iii));
        
        T(ii).maximo(:,iii) = dados_termo(indice_termica,3) * ... 
            (dados_termo(indice_termica,4)/100) * ...
            (1-(dados_termo(indice_termica,5)/100)) * ...
            (1-(dados_termo(indice_termica,6)/100));
            %              POT       *       
            %             (FCMX/100)       *      
            %             (1 - (TEIF/100))      * 
            %             (1 - (IP/100)) 

        indice_mes_final = 18;
        if length(tempo) + data_inicial(2) < 14
            indice_mes_final = 6 + data_inicial(2) + length(tempo) - 1;
        end
        
        T(ii).minimo(:,iii) = [dados_termo(indice_termica,6+data_inicial(2):indice_mes_final)';...  % Minimo do primeiro ano
            ones(size(tempo,1)-13+data_inicial(2),1)*dados_termo(indice_termica,19)]; % Minimo do demais anos
        

        indice_termica = find(dados_custo(:,1)==T(ii).codigo(iii));
        
        switch n_anos
            case 0
                T(ii).custo(:,iii) = [ ... % Custo do primeiro ano
                    ones(length(tempo),1)*dados_custo(indice_termica,3);...
                    ];
          
            case 1
                T(ii).custo(:,iii) = [ ...
                    ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
                    ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
                    ];
            case 2
                T(ii).custo(:,iii) = [ ...
                    ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
                    ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
                    ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
                    ];
            case 3
                 T(ii).custo(:,iii) = [ ...
                    ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
                    ones(12,1)*dados_custo(indice_termica,4);... % Custo do segundo ano
                    ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
                    ones(12,1)*dados_custo(indice_termica,6);...            % Custo do quarto ano
                    ];
            otherwise
                T(ii).custo(:,iii) = [ones(13-data_inicial(2),1)*dados_custo(indice_termica,3);... % Custo do primeiro ano
                    ones(12,1)*dados_custo(indice_termica,4);...            % Custo do segundo ano
                    ones(12,1)*dados_custo(indice_termica,5);...            % Custo do terceiro ano
                    ones(12,1)*dados_custo(indice_termica,6);...            % Custo do quarto ano
                    ones(12,1)*dados_custo(indice_termica,7)];              % Custo do quinto ano
                
        end
       

        
        T(ii).fonte = [ T(ii).fonte texto_custo(indice_termica,8)];
        
        if ~isempty(dados_custo1)
            indice_termica = find(dados_custo1(:,1)==T(ii).codigo(iii));
%             if ~isempty(indice_termica)
%                 T(ii).custo(1,iii) = dados_custo1(indice_termica,3);
%             end
        end
    end
end
T(indice) = T;

end

