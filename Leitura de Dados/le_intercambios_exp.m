function [ I_exp, p_I_exp, p_cons_I_exp ] = le_intercambios_exp( a, b, tempo, indice, N_submercado )
  %LE_INTERCAMBIOS Fun��o que l� os limites de interc�mbio do deck do newave
  %   Faz a leitura dos dados de limite de interc�mbio no excel do deck do
  %   newave escolhido pelo usu�rio e organiza esses dados de acordo com cada
  %   linha de interc�mbio. Separando por custo, m�ximo e m�nimo

  dados = a{1,33};
  texto = b{1,33};

  dados_expansao = a{1,34};

  numero_tempo = datenum(texto(2:end,1),'dd/mm/yyyy');
  [lixo,indice_tempo,ib] = intersect(numero_tempo,tempo);
  % dados = dados(indice_tempo,:);

  % Criando matrizes que armazenam limite de interc�mbio m�nimo
  % entre submercados. I.maximo -> A -> B. I.minimo B -> A.
  I_exp.maximo = zeros(size(tempo,1),size(dados,2)/2);
  I_exp.minimo = zeros(size(tempo,1),size(dados,2)/2);
  I_exp.linhas = zeros(size(I_exp.maximo,2));
  I_exp.nomes  = [];
  p_I_exp.nomes = [];
  p_I_exp.custo = zeros(size(tempo,1), size(dados,2)/2);
  p_I_exp.maximo = Inf*ones(size(tempo,1), size(dados,2)/2);
  p_I_exp.minimo = zeros(size(tempo,1), size(dados,2)/2);

  p_cons_I_exp.maximo = Inf*ones(size(tempo,1), size(dados,2)/2);
  p_cons_I_exp.minimo = zeros(size(tempo,1), size(dados,2)/2);
  p_cons_I_exp.nomes = [];
  p_cons_I_exp.custo = zeros(size(tempo,1), size(dados,2)/2);
  
  

  for ii = 1:size(I_exp.maximo,2)
    I_texto = char(texto(1,2*ii));
    I_submercado = findstr(I_texto,'_');
    I_imperatriz = findstr(I_texto,'_11');

    %     I_exp.maximo(:,ii) = dados(:,2*ii-1);
    I_exp.maximo(:,ii) = dados(2*ii-1)*ones(size(I_exp.maximo,1),1);
    I_exp.minimo(:,ii) = dados(2*ii)*ones(size(I_exp.maximo,1),1);
    p_cons_I_exp.custo(:,ii) = dados_expansao(:,ii)*ones(size(tempo,1),1);

    % Verifique se o interc�mbio � com a Imperatriz ou se o interc�mbio �
    % com um submercado
    if ~isempty(I_imperatriz)
      % Verifique se o interc�mbio � Imperatriz > Submercado ou
      % Submercado > Imperatriz.
      if I_submercado(1)==I_imperatriz
        I_exp.nomes = strvcat(I_exp.nomes,['FI' ...
        num2str(indice(I_texto(I_submercado(2)+1:end)))]);
        I_exp.linhas(5,ii) = -1;
        I_exp.linhas(indice(str2num(I_texto(I_submercado(2)+1:end))),ii) = 1;
      else
        I_exp.nomes = strvcat(I_exp.nomes,['F' ...
        num2str(indice(str2num(I_texto(I_submercado(1)+1:I_submercado(2)-1)))) 'I']);
        I_exp.linhas(indice(str2num(I_texto(I_submercado(1)+1:I_submercado(2)-1))),ii) = -1;
        I_exp.linhas(5,ii) = 1;
      end
    else
      I_texto(I_submercado) = [];
      I_exp.nomes = strvcat(I_exp.nomes,['F' num2str(indice(str2num(I_texto(2)))) ...
      num2str(indice(str2num(I_texto(3))))]);
      I_exp.linhas(str2num(I_exp.nomes(ii,2)),ii) = -1;
      I_exp.linhas(str2num(I_exp.nomes(ii,3)),ii) = 1;
    end
  end

  % Adicionando as vari�veis custo e n�mero de n�s a struct I
  I_exp.custo = zeros(size(I_exp.maximo));
  I_exp.N_nos = size(I_exp.maximo,2) - N_submercado;

  % Arrumar a ordem e a dire��o dos fluxos
  I_exp_temp = I_exp;
  p_I_exp_temp = p_I_exp;
  p_cons_I_exp_temp = p_cons_I_exp;
  I_exp_temp.nomes = strvcat('F1I', 'F2I', 'FI3', 'F23', 'F34');
  for ii=1:size(I_exp.linhas,1)
    indice_I_temp_nome = strmatch(I_exp_temp.nomes(ii,:),I_exp.nomes);
    if ~isempty(indice_I_temp_nome)
      I_exp_temp.maximo(:,ii) = I_exp.maximo(:,indice_I_temp_nome);
      I_exp_temp.minimo(:,ii) = I_exp.minimo(:,indice_I_temp_nome);
      I_exp_temp.custo(:,ii)  = I_exp.custo(:,indice_I_temp_nome);
      p_I_exp_temp.custo(:,ii) = p_I_exp.custo(:,indice_I_temp_nome);
      p_cons_I_exp_temp.custo(:,ii) = p_cons_I_exp.custo(:,indice_I_temp_nome);
    end
    indice_I_temp_nome = strmatch(I_exp_temp.nomes(ii,[1 3 2]),I_exp.nomes);
    if ~isempty(indice_I_temp_nome)
      I_exp_temp.maximo(:,ii) = I_exp.minimo(:,indice_I_temp_nome);
      I_exp_temp.minimo(:,ii) = I_exp.maximo(:,indice_I_temp_nome);
      I_exp_temp.custo(:,ii)  = I_exp.custo(:,indice_I_temp_nome);
      I_exp_temp.nomes(ii,:)  = I_exp.nomes(indice_I_temp_nome,[1 3 2]);
      p_I_exp_temp.custo(:,ii) = p_I_exp.custo(:,indice_I_temp_nome);
      p_cons_I_exp_temp.custo(:,ii) = p_cons_I_exp.custo(:,indice_I_temp_nome);
    end
  end

  I_exp = I_exp_temp;
  p_I_exp = p_I_exp_temp;
  p_cons_I_exp = p_cons_I_exp_temp;
  
  I_exp.minimo = -I_exp.minimo;
  I_exp.linhas =  [-1  0  0  0  0;
  0 -1  0 -1  0;
  0  0  1  1 -1;
  0  0  0  0  1;
  1  1 -1  0  0];


end
