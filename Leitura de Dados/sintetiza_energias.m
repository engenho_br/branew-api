function [tempo, energias, str_sub, mlt] = sintetiza_energias(deck_NW, eafpast, mes_corr, flag, str_sub)

%%% flag
% 1 -> Energias em MWmed
% 2 -> Energias em MLT


meses = {'JAN' 'FEV' 'MAR' 'ABR' 'MAI' 'JUN' 'JUL' 'AGO' 'SET' 'OUT' 'NOV' 'DEZ'};

% --------------------- Ler PARP - coeficientes e mlt ---------------------
file_parp = [deck_NW 'PARP.DAT'];
[mlt_conf, WC, mes, ano] = le_parp(file_parp, str_sub);

[lixo,mes] = ismember(mes,meses);
tempo = datenum([str2num(ano) mes ones(size(mes))]);

% ------------------ Ler PMO - Configura��o para cada m�s -----------------
file_pmo = [deck_NW 'PMO.DAT'];
[data_conf] = le_pmo(file_pmo);

%--------- Arrumar a mlt no m�s a m�s de acordo com a configura��o --------
mlt = [];
for a = 1:size(data_conf,1)
    for m = 2:size(data_conf,2)
        mlt = [mlt squeeze(mlt_conf(data_conf(a,m),m-1,:))];
    end
end

if flag == 2
    eafpast = eafpast.*mlt(:,1:12);
end

% --------------------- Sintetizar M�dia dos cen�rios ---------------------
load ruidos.mat

n_ruidos = size(R(mes_corr).ruidos,2);

energias = zeros(length(str_sub),length(tempo),n_ruidos);
ruidos   = zeros(1,length(tempo),n_ruidos);
for ii = 1:length(str_sub)
    wc = WC(ii).wc';
    energias(ii,:,1) = SimPARP2(eafpast(ii,:), mlt(ii,:), mlt_conf(1,:,ii), wc, mes_corr)';
    
    energias(ii,:,:) = repmat(energias(ii,:,1),[1 1 n_ruidos]);
    
    ruidos(1,:,:) = R(mes_corr).ruidos(:,:,ii);
    energias(ii,mes_corr:end,:) = energias(ii,mes_corr:end,:) + ruidos(1,mes_corr:end,:);
end

energias = energias(:,mes_corr:end,:);
tempo    = tempo(mes_corr:end);
mlt      = mlt(:,mes_corr:end);

energias(energias<0) = 0;

