function [ I ] = le_intercambios( a, b, tempo, indice, N_submercado )
%LE_INTERCAMBIOS Fun��o que l� os limites de interc�mbio do deck do newave
%   Faz a leitura dos dados de limite de interc�mbio no excel do deck do
%   newave escolhido pelo usu�rio e organiza esses dados de acordo com cada
%   linha de interc�mbio. Separando por custo, m�ximo e m�nimo

dados = a{1,2};
texto = b{1,2};

numero_tempo = datenum(texto(2:end,1),'dd/mm/yyyy');
[lixo,indice_tempo,ib] = intersect(numero_tempo,tempo);
dados = dados(indice_tempo,:);

% Criando matrizes que armazenam limite de interc�mbio m�nimo
% entre submercados. I.maximo -> A -> B. I.minimo B -> A.
I.maximo = zeros(size(tempo,1),size(dados,2)/2);
I.minimo = zeros(size(tempo,1),size(dados,2)/2);
I.linhas = zeros(size(I.maximo,2));
I.nomes  = [];


for ii = 1:size(I.maximo,2)
    I_texto = char(texto(1,2*ii));
    I_submercado = findstr(I_texto,'_');
    I_imperatriz = findstr(I_texto,'_11');
    
    I.maximo(:,ii) = dados(:,2*ii-1);
    I.minimo(:,ii) = dados(:,2*ii);
    
    % Verifique se o interc�mbio � com a Imperatriz ou se o interc�mbio �
    % com um submercado
    if ~isempty(I_imperatriz)
        % Verifique se o interc�mbio � Imperatriz > Submercado ou 
        % Submercado > Imperatriz.
        if I_submercado(1)==I_imperatriz
            I.nomes = strvcat(I.nomes,['FI' ...
                num2str(indice(I_texto(I_submercado(2)+1:end)))]);
            I.linhas(5,ii) = -1;
            I.linhas(indice(str2num(I_texto(I_submercado(2)+1:end))),ii) = 1;
        else
            I.nomes = strvcat(I.nomes,['F' ...
                num2str(indice(str2num(I_texto(I_submercado(1)+1:I_submercado(2)-1)))) 'I']);
            I.linhas(indice(str2num(I_texto(I_submercado(1)+1:I_submercado(2)-1))),ii) = -1;
            I.linhas(5,ii) = 1;
        end
    else
        I_texto(I_submercado) = [];
        I.nomes = strvcat(I.nomes,['F' num2str(indice(str2num(I_texto(2)))) ...
            num2str(indice(str2num(I_texto(3))))]);
        I.linhas(str2num(I.nomes(ii,2)),ii) = -1;
        I.linhas(str2num(I.nomes(ii,3)),ii) = 1;
    end
end

% Adicionando as vari�veis custo e n�mero de n�s a struct I 
I.custo = zeros(size(I.maximo));
I.N_nos = size(I.maximo,2) - N_submercado;

% Arrumar a ordem e a dire��o dos fluxos
I_temp = I;
I_temp.nomes = strvcat('F1I', 'F2I', 'FI3', 'F23', 'F34');
for ii=1:size(I.linhas,1)
    indice_I_temp_nome = strmatch(I_temp.nomes(ii,:),I.nomes);
    if ~isempty(indice_I_temp_nome)
        I_temp.maximo(:,ii) = I.maximo(:,indice_I_temp_nome);
        I_temp.minimo(:,ii) = I.minimo(:,indice_I_temp_nome);
        I_temp.custo(:,ii)  = I.custo(:,indice_I_temp_nome);
    end
    indice_I_temp_nome = strmatch(I_temp.nomes(ii,[1 3 2]),I.nomes);
    if ~isempty(indice_I_temp_nome)
        I_temp.maximo(:,ii) = I.minimo(:,indice_I_temp_nome);
        I_temp.minimo(:,ii) = I.maximo(:,indice_I_temp_nome);
        I_temp.custo(:,ii)  = I.custo(:,indice_I_temp_nome);
        I_temp.nomes(ii,:)  = I.nomes(indice_I_temp_nome,[1 3 2]);
    end
end
I = I_temp;

I.minimo = -I.minimo;
I.linhas =  [-1  0  0  0  0;
              0 -1  0 -1  0;
              0  0  1  1 -1;
              0  0  0  0  1;
              1  1 -1  0  0];


end

