function [ Q_exp, p_GH_exp, V_exp, p_cons_GH_exp ] = le_dados_hidraulica_expansao( a, tempo, indice, N_submercado )
  %LE_DADOS_HIDRAULICA Leitura de dados sobre as informa��es relacionadas as
  %hidraulicas
  %   Leitura de configura��o hidraulica, de dados de hidr�ulicas e de
  %   expans�o de hidraulicas do deck newave escolhido. Com essas informa��es
  %   ela calcula o turbinamento m�ximo, minimo e seu custo para cada
  %   subsistema.

  %  5 Configuracao Hidraulica
  % 18 Dados Hidraulica

  % Configura��o Hidr�ulica
  cod_hidro = a{36};

  %Dados Hidr�ulicas
  dados_hidro = a{35};

  %Recuperando dados de pot�ncia
  if ~isempty(dados_hidro)
    pot_hidro = repmat(dados_hidro(:,2),1,size(tempo,1));
  else
    pot_hidro = [];
  end

  for i = 1:N_submercado
    if ~isempty(cod_hidro)
      hidraulicas(i).numero = size(find(cod_hidro(:,3) == i),1);
    else
      hidraulicas(i).numero = 0;
    end

    hidraulicas(i).q_maximo = [];
    hidraulicas(i).q_minimo = [];
    hidraulicas(i).v_maximo = [];
    hidraulicas(i).gh_custo = [];
    hidraulicas(i).tempo_construcao= [];

    if hidraulicas(i).numero == 0
      hidraulicas(i).q_maximo = zeros(0, length(tempo));
      hidraulicas(i).q_minimo = zeros(0, length(tempo));
      hidraulicas(i).v_maximo = zeros(0, length(tempo));
      hidraulicas(i).gh_custo = zeros(0, length(tempo));
      hidraulicas(i).tempo_construcao = zeros(0, 0);
    end
  end

  q_maximo = [];

  %Calculando capacidade m�xima
  for ii = 1:size(pot_hidro,1)
    sub = cod_hidro(ii,3);
    %                                                      POT       *       (1-(TEIF/100))     *        (1 - (IP/100))
    hidraulicas(sub).q_maximo = [hidraulicas(sub).q_maximo;...
    (pot_hidro(ii,:)*(1-(dados_hidro(ii,3)/100))*(1-(dados_hidro(ii,4)/100)))];
    hidraulicas(sub).v_maximo = [hidraulicas(sub).v_maximo;...
    repmat(dados_hidro(ii,6),1,size(tempo,1))];
    hidraulicas(sub).gh_custo = [hidraulicas(sub).gh_custo;...
    repmat(dados_hidro(ii,7),1,size(tempo,1))];
    hidraulicas(sub).tempo_construcao = [hidraulicas(sub).tempo_construcao
    dados_hidro(ii,8)];
  end

  hidraulicas(indice) = hidraulicas;

  % Atualize o turbinamento m�ximo, custo e turbinamento minimo de cada submercado
  for ii = 1:N_submercado
    p_GH_exp(ii).custo = zeros(size(hidraulicas(ii).gh_custo));
    p_GH_exp(ii).minimo = zeros(size(hidraulicas(ii).q_maximo));
    p_GH_exp(ii).maximo = Inf*ones(size(hidraulicas(ii).q_maximo));
    p_GH_exp(ii).tempo_construcao = hidraulicas(ii).tempo_construcao;
    p_cons_GH_exp(ii).maximo = Inf*ones(size(hidraulicas(ii).q_maximo));
    p_cons_GH_exp(ii).minimo = zeros(size(hidraulicas(ii).q_maximo));
    p_cons_GH_exp(ii).custo = hidraulicas(ii).gh_custo;
    V_exp(ii).maximo = hidraulicas(ii).v_maximo;
    V_exp(ii).minimo = zeros(size(hidraulicas(ii).v_maximo));
    V_exp(ii).numero = hidraulicas(ii).numero;
    Q_exp(ii).maximo = hidraulicas(ii).q_maximo;
    Q_exp(ii).custo  = zeros(size(Q_exp(ii).maximo));
    Q_exp(ii).minimo = zeros(size(Q_exp(ii).maximo));
    Q_exp(ii).numero = hidraulicas(ii).numero;
  end
end
