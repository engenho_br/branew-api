function [tempo, dados, Nsub, Ncen] = le_energiaF(arquivo, mes, ano)

%%% Abre, l� e fecha o arquivo de energia F
fid1 = fopen(arquivo);
[filename, mode, machineformat] = fopen(fid1);
if fid1==-1,
    tempo=[];
    dados=[];
    Nsub=[];
    Ncen=[];
    
else
    rawF = fread(fid1,'float64');
    fclose(fid1);

    %%% Completa com zeros o final do arquivo caso seja necess�rio completar o
    %%% �ltimo bloco (blocos de 3000 registros)
    if ano < 2013
        N = 3000;
    else
        N = 4500;
    end
    F = [rawF; zeros(N-mod(length(rawF),N),1)];

    %%% Ajeita os dados em matriz
    F = reshape(F,[N length(F)/N]);
    F = F';

    %%% Separa os submercados
    Nsub = length(unique(F(1,F(1,:)~=0)));
    Ncen = length(find(F(1,:) == F(1,1)));
    clear rawF;
    for ii = 1:Nsub
        dados(ii,:,:) = F(:,1:Ncen);

        F(:,1:Ncen) = [];
    end

    tempo = datevec([datenum(ano-1,7,1): datenum(ano+floor(size(F,1)/12)-1,12,1)]);
    tempo(tempo(:,3)~=1,:) = [];
    tempo = datenum(tempo);

    %%% Cortar dados e tempo
    tempo(1:6) = [];
    tempo(61:end) = [];
    tempo(1:mes-1) = [];


    dados(:,1:6,:) = [];
    dados(:,61:end,:) = [];
    dados(:,1:mes-1,:) = [];

end
