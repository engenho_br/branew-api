function [ T, Folga ] = le_termica_deficit( a, indice, T, tempo, D, N, N_submercado )
%LE_TERMICA_DEFICIT Leitura dos dados de t�rmicas de deficit
%   Faz a leitura dos dados de t�rmicas de d�ficit do excel do deck do
%   newave e adiciona estes dados aos dados de t�rmica j� guardados
%   anteriormente. Esta fun��o tamb�m gera os dados das vari�veis de
%   Folga(custo,m�ximo e m�nimo).

def(:,indice)   = a{1,27};
custo_deficit   = def(2:5,:);
patamar_deficit = def(6:9,:);

N_def = size(patamar_deficit,1);

for ii = 1:N_submercado
    T(ii).codigo = [T(ii).codigo; ones(N_def,1)+[1:N_def]'];
    T(ii).numero = T(ii).numero + N_def;
    T(ii).nome   = strvcat(T(ii).nome,['Deficit1';'Deficit2';'Deficit3' ...
                                        ;'Deficit4']);
    T(ii).maximo = [T(ii).maximo [patamar_deficit(:,ii)*D(ii,:,1)]'];
    T(ii).minimo = [T(ii).minimo zeros(N,N_def)];
    T(ii).custo  = [T(ii).custo  repmat(custo_deficit(:,ii)',N,1)];
    
    Folga(ii).minimo = zeros(N,1);
    Folga(ii).maximo = ones(N,1)*Inf;
    Folga(ii).custo  = 10*ones(N,1)*custo_deficit(end,ii);
    
    dif_potencias = T(ii).maximo-T(ii).minimo;
    indice_termicas = find(dif_potencias<0);
    if ~isempty(indice_termicas)
        T(ii).minimo(indice_termicas)=T(ii).maximo(indice_termicas);
        [lin,col] = ind2sub(size(T(ii).maximo),indice_termicas);

    end
end



end

