function handles = le_dados(handles)
handles.leitura_errada = 0;

N_submercado = handles.N_sub;

sheets = {'Dados Gerais'                 % 1
    'Limites de Intercambio'             % 2
    'Mercado de Energia'                 % 3
    'Geracao Pequenas Usinas'            % 4
    'Configuracao Hidraulica'            % 5
    'Configuracao Termica'               % 6
    'Dados das Termicas'                 % 7
    'Custo da Termica'                   % 8
    'Custo da Termica m�s 1'             % 9
    'Expansao Termica'                   % 10
    'Patamares de Carga'                 % 11
    'Patamares de Demanda(PU)'           % 12
    'Patamares de Intercambio(PU)'       % 13
    'Manutencao prog Termicas'           % 14
    'Tendencia Hidrologica'              % 15
    'Tendencia Hidrologica'              % 16
    'Expansao Hidraulica'                % 17
    'Dados Hidraulica'                   % 18
    'Energia Afluente Norte'             % 19
    'Energia Afluente Nordeste'          % 20
    'Energia Afluente Sudeste'           % 21
    'Energia Afluente Sul'               % 22
    'Energia Fio Norte'                  % 23
    'Energia Fio Nordeste'               % 24
    'Energia Fio Sudeste'                % 25
    'Energia Fio Sul'                    % 26
    'Deficit'                            % 27
    'Dados das Hidr�ulicas'              % 28
    'Volumes'                            % 29
    'Configuracao Termica - Expans�o'    % 30
    'Dados das Termicas - Expans�o'      % 31
    'Custo da Termica - Expans�o'        % 32
    'Limites de Intercambio - Expans�o'  % 33
    'Custo de Expans�o de Intercambio'   % 34
    'Dados Hidraulica - Expans�o'        % 35
    'Configuracao Hidraulica - Expans�o' % 36
    };

% h = waitbar(0,['Carregando ']);

%Diret�rio aonde est� as planilhas com os dados
diretorio = handles.edit_folder_deck;

%Leitura de dados
% Caso n�o consiga ler, mostre uma dialog box com o erro para o cliente
try
    size_sheets = 1:size(sheets,1);
    
    for ii = size_sheets
%         waitbar(ii*(1/length(size_sheets)))
        [a{ii},b{ii}] = xlsread([diretorio '\' char(sheets{ii}) '.xls']);
    end
    
catch
    warndlg('Deck de dados compativel n�o encontrado neste diret�rio!')
    handles.leitura_errada = 1;
    try close(h); catch end
    return
end

try close(h); catch end

% ------------------------ 15 Tend�ncia Hidrologica -----------------------
% % Para os Nossos Fluxos
% %  1 - Norte
% %  2 - Nordeste
% %  3 - Sudeste
% %  4 - Sul
dados = a{1,15};
texto = b{1,15};

[nomes_sub, eafpast,lixo,indice] = le_tendencia_hidrologica(dados, texto, ...
                                                            handles);

% ----------------------------- 1 Dados Gerais ----------------------------

[N, v_inicial, tempo, prob_cenario, tx_desc, mes_inicial, ano_inicial, n_anos] = ...
    le_dados_gerais(a, b, indice, handles);

% ----------------------- 4 Geracao Pequenas Usinas -----------------------
dados = a{1,4};
texto = b{1,4};
indice_tempo = find(datenum(char(texto(2:end,1)),'dd/mm/yyyy')>=tempo(1) & ...
    datenum(char(texto(2:end,1)),'dd/mm/yyyy')<=tempo(end));
dados_peq_usinas(:,indice) = dados(indice_tempo+1,:);

% ------------------------- 3 Mercado de Energia --------------------------
dados = a{1,3};
texto = b{1,3};
indice_tempo = find(datenum(char(texto(2:end,1)),'dd/mm/yyyy')>=tempo(1) & ...
    datenum(char(texto(2:end,1)),'dd/mm/yyyy')<=tempo(end));

% Adquirindo demanda do mercado e salvando na vari�vel D
D(:,indice,1) = dados(indice_tempo+1,:);

D = (D - dados_peq_usinas)';

% ----------------------------- Turbinamento ------------------------------

[Q] = le_dados_hidraulica(a, b, tempo, indice, N_submercado, N);

% ------------------- Expans�o de Hidraulica ------------------------------

[Q_exp, p_GH_exp, V_exp, p_cons_GH_exp] = le_dados_hidraulica_expansao(a, tempo, indice, N_submercado);
% ----------------------- 2 Limites de Intercambio ------------------------

[I] = le_intercambios(a, b, tempo, indice, N_submercado);          

% ----------------------- Expans�o dos Limites de Intercambio -------------

[I_exp, p_I_exp, p_cons_I_exp] = le_intercambios_exp(a, b, tempo, indice, N_submercado);

% -------------------- Volumes e Vertimentos ---------------------

% Volumes m�ximo e m�nimo por submercado
dados_volume = a{29};

for ii = 1:N_submercado
  
    V(ii).maximo  = ones(N,1)*dados_volume(ii,3);
    V(ii).minimo = ones(N,1)*dados_volume(ii,2);
    
    S(ii).custo  = zeros(N,1);
    S(ii).minimo = zeros(N,1);
    S(ii).maximo = ones(N,1)*Inf;
end

V(indice) = V;

for ii = 1:N_submercado
    V(ii).inicial = v_inicial(ii);
end

% ------------------------ 6 Configurac�o T�rmica -------------------------
% ------------------------ 7 Dados das T�rmicas ---------------------------
% ------------------------ 8 Custo da T�rmica -----------------------------
% ------------------------ 9 Custo da T�rmica 1 ---------------------------

[T, legenda, dados_termo] = le_termicas(a, b, indice, N_submercado, tempo, n_anos);


% ------------------------ 10 Expansao Termica ----------------------------

[T] = le_expansao_termica(a, b, legenda, dados_termo, tempo, indice, T, N_submercado);


% ---------------------- T�rmicas de D�ficit e Folga ----------------------

[T, Folga] = le_termica_deficit(a, indice, T, tempo, D, N, N_submercado);

% ---------------------- T�rmicas de Expans�o------------------------------

[p_T_exp, T_exp, p_cons_T_exp] = le_termicas_de_expansao(a, b, indice, N_submercado, tempo, n_anos);

% --------------------------- 19 - 26 Aflu�ncias --------------------------

[tempo_A, A, EF, num_cenario, prob_cenario] = ...
    le_afluencias(a, handles, diretorio, mes_inicial, ano_inicial, nomes_sub, prob_cenario, ...
    eafpast, indice, N_submercado, Q, N);

% ------------------------------ MWmed -> MWh -----------------------------


% ---------------------- Colocar Dados nas Estruturas ---------------------
handles.tempo = tempo;
handles.N     = N;

handles.num_cen   = num_cenario;
handles.prob_cen  = prob_cenario;
% handles.horas_aux = horas_aux;
handles.tx_desc   = tx_desc;

handles.I = I;
handles.T = T;
handles.V = V;
handles.S = S;
handles.Q = Q;
handles.D = D;
handles.A = A;
handles.EF = EF;
handles.T_exp = T_exp;
handles.p_T_exp = p_T_exp;
handles.I_exp = I_exp;
handles.p_I_exp = p_I_exp;
handles.Q_exp = Q_exp;
handles.V_exp = V_exp;
handles.p_GH_exp = p_GH_exp;
handles.p_cons_GH_exp = p_cons_GH_exp;
handles.p_cons_I_exp = p_cons_I_exp;
handles.p_cons_T_exp = p_cons_T_exp;

handles.Folga = Folga;
% 
% set(handles.instantes,'String',datestr(tempo,'dd/mm/yyyy'));
% set(handles.cenario,'String',num2str([1:num_cenario]'));
% set(handles.list_cen,'String',num2str([1:num_cenario]'));

% guidata(handles.figure1,handles);
