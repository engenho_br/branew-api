function [dados] = le_engfio(arquivo, mes, ano, num_sub, num_meses)

%%% Abre, l� e fecha o arquivo de energia F
fid1 = fopen(arquivo);
[filename, mode, machineformat] = fopen(fid1);
if fid1==-1,
    tempo=[];
    dados=[];
    Nsub=[];
    Ncen=[];
    
else
    rawF = fread(fid1,'float64');
    fclose(fid1);

    %%% Completa com zeros o final do arquivo caso seja necess�rio completar o
    %%% �ltimo bloco (blocos de 3000 registros)
    if ano < 2013
        N = 3000;
    else
        N = 4500;
    end
    num_sub = 4;
    temp = reshape(rawF,12,length(rawF)/12);
    ndx3 = find(sum(temp,1)~=0);
    temp = temp(:,ndx3)';
    
    num_hist = length(temp)/num_meses/num_sub;
    
%     temp = reshape(temp, 12, num_hist, newdim);
    
    for cnt_mes=1:num_meses
        for cnt_sub=1:num_sub
            dados.fio(cnt_sub,cnt_mes).val = temp(1:num_hist,:);
            temp(1:num_hist,:) = [];
        end
    end
    
end
