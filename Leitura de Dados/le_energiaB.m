function [tempo, dados, Nsub, num_cen] = le_energiaB(arquivo, mes, ano, num_cen)

%%% Abre, l� e fecha o arquivo de energia B
fid1 = fopen(arquivo);
[filename, mode, machineformat] = fopen(fid1);
if fid1==-1,
    tempo=[];
    dados=[];
    Nsub=[];
    Ncen=[];
    
else
    B = fread(fid1,'float64');
    fclose(fid1);

    %%% Completa com zeros o final do arquivo caso seja necess�rio completar o
    %%% �ltimo bloco (blocos de 3000 registros)
    N = 4*30000;
    B = [B; zeros(N-mod(length(B),N),1)];

    %%% Ajeita os dados em matriz
    B = reshape(B,[N length(B)/N]);
    B = B';

    %%% Separa os submercados
    Nsub = length(unique(B(1,B(1,:)~=0)));
    Ncen = length(find(B(1,:) == B(1,1)));

    for ii = 1:Nsub
        dados(ii,:,:) = B(:,1:Ncen);

        B(:,1:Ncen) = [];
    end

    tempo = datevec([datenum(ano,1,1): datenum(ano+floor(size(B,1)/12)-1,12,1)]);
    tempo(tempo(:,3)~=1,:) = [];
    tempo = datenum(tempo);

    %%% Cortar dados e tempo
    tempo(61:end) = [];
    tempo(1:mes-1) = [];

    dados(:,61:end,:) = [];
    dados(:,1:mes-1,:) = [];
end

%%% selecionar num_cen
[lixo,ia] = sort(squeeze(sum(sum(dados(:,1:12,:),1),2)));
ia = ia(round(linspace(1,Ncen,num_cen)));

dados = dados(:,:,ia);




