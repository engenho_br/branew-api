function [ T ] = le_expansao_termica( a, b, legenda, dados_termo, tempo, ...
    indice, T, N_submercado )
%LE_EXPANSAO_TERMICA Faz a leitura de dados de expans�o t�rmica do deck do
%newave
%   Faz a leitura de dados de expans�o t�rmica do excel do deck do newave e
%   atualiza as informa��es de cada t�rmica para cada cen�rio de acordo com
%   os dados do planejamento de expans�o de t�rmicas.

dados = a{1,10};
texto = b{1,10};

% POTEF=1, FCMAX=2, IPTER=3, GTMIN=4
tipo = texto(2:end,2);
p_codificar = {'POTEF';'FCMAX';'IPTER';'GTMIN'};
[lixo,tipo] = ismember(tipo,p_codificar);

% Verificar se h� expans�o termica planejada
if size(dados, 1) == 0
    cod_usinas = [];
else
    % Encontrar todos os codigos das usinas que vao sofrer expansao
    cod_usinas = unique(dados(:,1));
end



for ii = 1:size(cod_usinas,1)
    indice_termo = find(dados_termo(:,1)==cod_usinas(ii));
    
    aux_sub   = legenda(find(legenda(:,1)==cod_usinas(ii)),2);
    aux_usina = find(T(indice(aux_sub)).codigo==cod_usinas(ii));
    
    aux_expansao = [repmat(dados_termo(indice_termo,3),size(tempo))... % POT
        repmat(dados_termo(indice_termo,4),size(tempo))... % FCMAX
        repmat(dados_termo(indice_termo,6),size(tempo))... % IPTER
        T(indice(aux_sub)).minimo(:,aux_usina)... % GTMIN
        repmat(dados_termo(indice_termo,5),size(tempo))];  % TEIF
    
    for tp = 1:N_submercado
        indice_termo = intersect(find(dados(:,1)==cod_usinas(ii)),find(tipo==tp));
        if ~isempty(indice_termo)
            for iii = 1:size(indice_termo,1)
                % dados_termo(:,3) -> POT
                % dados_termo(:,4) -> FCMX
                % dados_termo(:,5) -> TEIF
                % dados_termo(:,6) -> IP
                if ~isnan(dados(indice_termo(iii),4)) && ~isnan(dados(indice_termo(iii),5))
                    data_i = datenum(dados(indice_termo(iii),5),dados(indice_termo(iii),4),1);
                else
                    data_i = tempo(1);
                end
                
                if ~isnan(dados(indice_termo(iii),6)) && ~isnan(dados(indice_termo(iii),7))
                    data_final = datenum(dados(indice_termo(iii),7),dados(indice_termo(iii),6),1);
                else
                    data_final = tempo(end);
                end
                
                indice_tempo = find(tempo>=data_i & tempo<=data_final);
                aux_expansao(indice_tempo,tp) = dados(indice_termo(iii),3);
            end
        end
    end
    T(indice(aux_sub)).minimo(:,aux_usina) = aux_expansao(:,4);
    T(indice(aux_sub)).maximo(:,aux_usina) = ...
        aux_expansao(:,1).*(aux_expansao(:,2)/100).* ...
        (1-(aux_expansao(:,3)/100)).*(1-(aux_expansao(:,5)/100));
    
    dif_potencias = T(indice(aux_sub)).maximo(:,aux_usina) - ...
        T(indice(aux_sub)).minimo(:,aux_usina);
    if any(dif_potencias<0)
        T(indice(aux_sub)).minimo(dif_potencias<0,aux_usina) = ...
            T(indice(aux_sub)).maximo(dif_potencias<0,aux_usina);
    end
end

end

