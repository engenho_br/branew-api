function [ nomes_sub, eafpast,lixo,indice ] = le_tendencia_hidrologica( dados, texto, handles )
%LE_TENDENCIA_HIDROL�GICA Fun��o que retorna a tend�ncia hidrol�gica de um
% deck do Newave
%   A fun��o l� o arquivo excel de tend�ncia hidrol�gica do deck do newave
%   escolhido e retorna as informa��es de energia de aflu�ncia

nomes_sub = texto(2:end,2);
[lixo,indice] = ismember(nomes_sub,handles.nosso_fluxo);

eafpast = dados(:,3:end);


end

