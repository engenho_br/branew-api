function [ Q ] = le_dados_hidraulica( a, b, tempo, indice, N_submercado, N )
%LE_DADOS_HIDRAULICA Leitura de dados sobre as informa��es relacionadas as
%hidraulicas
%   Leitura de configura��o hidr�ulica, de dados de hidr�ulicas e de
%   expans�o de hidr�ulicas do deck newave escolhido. Com essas informa��es
%   ela calcula o turbinamento m�ximo, m�nimo e seu custo para cada
%   subsistema.

%  5 Configuracao Hidraulica
% 18 Dados Hidraulica

% Configura��o Hidr�ulica
cod_hidro = a{5};
status_exp = b{5}(2:end,7);
cod_hidro(:,[2 4 [6:11]]) = [];

%Dados Hidr�ulicas
dados_hidro = a{18};
dados_hidro(:,[2 3 4 [6:9]]) = [];

%Recuperando dados de pot�ncia
pot_hidro = repmat(dados_hidro(:,2),1,size(tempo,1));

maximo = zeros(4,size(tempo,1));

%Calculando capacidade m�xima
for ii = 1:size(pot_hidro,1)
    if strcmpi(status_exp(ii),'EX')
        %                                                      POT       *       (1-(TEIF/100))     *        (1 - (IP/100))
        maximo(cod_hidro(ii,3),:) = maximo(cod_hidro(ii,3),:)+(pot_hidro(ii,:)*(1-(dados_hidro(ii,3)/100))*(1-(dados_hidro(ii,4)/100)));
    end
end

% 17 Expansao Hidraulica
exp_hidro = a{17};
texto_exp  = b{17};

% Caso a data de in�cio do enchimento n�o esteja definida, use a data
% padr�o(tempo(1))
datas_vazia = strmatch(' ',char(texto_exp(:,3)));
texto_exp(datas_vazia,3) = {datestr(tempo(1),'dd/mm/yyyy')};

% Retirada das expans�es que n�o tem data entrada definida
datas_vazia = strmatch(' ',char(texto_exp(:,6)));
exp_hidro(datas_vazia-1,:) = [];
texto_exp(datas_vazia,:) = [];


exp_hidro(:,3) = datenum(char(texto_exp(2:end,3)),'dd/mm/yyyy');
exp_hidro(:,6) = datenum(char(texto_exp(2:end,6)),'dd/mm/yyyy');


while ~isempty(exp_hidro)
    %Pegue o indice da usina que tem expans�es planejadas 
    cod_usina  = find(cod_hidro(:,1)==exp_hidro(1,1));
    usinas = find(exp_hidro(:,1)==exp_hidro(1,1));
    if ~isempty(cod_usina)
        % Para cada usina que tem expans�es planejada, verifique se cada
        % expans�o j� ocorreu. Se ocorreu, atualize as informa��es de
        % turbinamento m�ximo
        for ii=1:size(usinas,1)
            aux2 = find(tempo>=exp_hidro(usinas(ii),6));
            maximo(cod_hidro(cod_usina,3),aux2) = ...
                maximo(cod_hidro(cod_usina,3),aux2) + ...
                (repmat(exp_hidro(usinas(ii),7),size(aux2')) * ... 
                (1-(dados_hidro(cod_usina,3)/100)) * ... 
                (1-(dados_hidro(cod_usina,4)/100)));
            %                    POT                     * 
            % (1-(TEIF/100))      *        (1 - (IP/100))
        end
    end
    %Ap�s verificar as expans�es da usina, retire-a da lista
    exp_hidro(usinas,:)=[];
end

maximo(indice,:) = maximo;

% Atualize o turbinamento m�ximo, custo e turbinamento minimo de cada submercado
for ii = 1:N_submercado
    Q(ii).maximo = maximo(ii,:)';
    Q(ii).custo  = zeros(N,1);
    Q(ii).minimo = zeros(N,1);
end


end

