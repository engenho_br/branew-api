function [datas_conf] = le_pmo(file_pmo)

A = strjust(char(textread(file_pmo, '%s', 'delimiter', '\n','whitespace','')),'left');

I1 = strmatch('CONFIGURACOES POR ENTRADA DE RESERVATORIO',A);
A = A(I1+6:end,:);

aux = str2num(A(1,:));
datas_conf = [];

while ~isempty(aux)
    datas_conf = [datas_conf; aux];
    A(1,:) = [];
    aux = str2num(A(1,:));
end