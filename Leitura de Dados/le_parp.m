function [mlt_conf, WC, mes, ano] = le_parp(file_parp, sub)

A = strjust(char(textread(file_parp, '%s', 'delimiter', '\n','whitespace','')),'left');

%%% Descobrir In�cio dos blocos de cada submercados
I1 = [];
for ii = 1:length(sub)
    I1 = [I1 strmatch(['SERIE  DE  ENERGIAS DO SISTEMA ' sub{ii}],A)];
end

mlt_conf = zeros(size(I1,1),12,length(sub));

for ii = 1:length(sub)
    %%% Procurar pelas MLTs
    I2 = strmatch('MEDIA AMOSTRAL DAS ENERGIAS',A);
    
    I2 = I2(I2>=I1(1,ii));
    if ii ~= length(sub)
        I2 = I2(I2<I1(1,ii+1));
    end
    
    mlt_conf(:,:,ii) = str2num(A(I2+5,:));
    
    %%% Procurar pelos coeficientes do PAR
    I2 = strmatch('PERIODO:',A);
    
    I2 = I2(I2>=I1(1,ii));
    if ii ~= length(sub)
        I2 = I2(I2<I1(1,ii+1));
    end
    
    [mes,ano] = strtok(cellstr(strtrim(strjust(A(I2,10:end),'left'))),'/');
    ano = char(ano);
    ano = ano(:,2:end);
    
    I = strmatch('PRE',ano); ano(I,:) = []; mes(I) = []; I2(I) = [];
    I = strmatch('POS',ano); ano(I,:) = []; mes(I) = []; I2(I) = [];
    
    aux = strtrim(A(I2+3,:));
    N = size(aux,1);
    
    wc = zeros(N,1);
    for iii = 1:N
        aux2 = str2num(aux(iii,:));
        
        wc = [wc zeros(N,length(aux2)-size(wc,2))];
        wc(iii,1:length(aux2)) = aux2;
    end
    
    WC(ii).wc = wc;
end