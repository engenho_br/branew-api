function [tempo_A, A, EF, num_cen, prob_cen] = le_afluencias(a, handles , diretorio, mes_i, ano_i, nomes_sub, prob_cen, eafpast, indice, N_sub, Q,N)

tempo_A = [];
%%% Leitura do hist�rico
% if get(handles.rd_historico, 'Value')
if handles.rd_historico
    for ii = 1:N_sub
        %%%   Energia Control�vel     + Energia Fio
        aux = a{18+ii}(mes_i+1:end,:) + a{22+ii}(mes_i+1:end,:);
        A(ii,:,:) = aux;
    end
end
% 
% %%% Leitura do EnergiaF
% if get(handles.rd_energiaf, 'Value')
%     [tempo_A, A, Nsub, Ncen] = le_energiaF([diretorio '\energiaf.dat'], mes_i, ano_i);
%     A(indice,:,:) = A;
% end

% %%% Leitura do EnergiaB
% if get(handles.rd_energiab, 'Value')
%     [tempo_A, A, Nsub, Ncen] = le_energiaB([diretorio '\energiab.dat'], mes_i, ano_i, 200);
%     A(indice,:,:) = A;
% end
% 
%%% Pelo C�lculo utilizando eafpast e PAR
% if get(handles.rd_eafpast, 'Value')
if handles.rd_eafpast
    [tempo_A, A, str_sub, mlt] = sintetiza_energias([diretorio '\'], eafpast, mes_i, get(handles.pop_eafpast, 'Value'), nomes_sub);
    A(indice,:,:) = A;
    mlt(indice,:) = mlt;
end
% 
%%% Verificar se as probabilidades condiz com o n�mero de cen�rios
num_cen = size(A,3);

if length(prob_cen)~=num_cen
    prob_cen = ones(1,num_cen)/num_cen;
end

% ------------------------------ Energia Fio ------------------------------
EF = zeros(size(A));
% if exist([cd '\Percentual Energia Fio.xls'],'file')
%     [p_ef,auxt] = xlsread([cd '\Percentual Energia Fio.xls']);
%     
%     p_ef = p_ef(mes_i:end,3:end);
%     p_ef(:,indice) = p_ef;
%     
%     for ii = 1:N_sub
%         max_Q = squeeze(repmat(Q(ii).maximo',[1 1 num_cen]));
%         aux_p_ef = repmat(p_ef(:,ii)',[1 1 size(A,3)]);
%         aux_p_ef = aux_p_ef(:,1:N,:);
%         aux_A = squeeze(A(ii,:,:).*aux_p_ef);
%         
%         aux = min(max_Q*0.95,aux_A);
%         
%         EF(ii,:,:) = aux;
%     end
end

