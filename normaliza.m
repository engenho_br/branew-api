function a=normaliza(A)
a=[];
if size(A,2)>1 & size(A,1)>1    %se for matriz
    for ii=1:size(A,2);
        b=A(:,ii);
        b=b-mean(b);
        b=b/sqrt(b'*b);
        a=[a b];
    end
else
    if size(A,2)>size(A,1)
        A=A.';
    end
    A=A-mean(A);
    A=A/sqrt(A'*A);
    a=A;
end