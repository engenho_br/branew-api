function pass3 = SimPARP2(eafpast, mlt, mlt_conf1, wc, mes)

% eafpast - energias passadas em modo circular
%         - sempre deve ter 12 colunas com as �ltimas 12 energias e sempre
%           na ordem de janeiro a dezembro
%
% mlt     - MLT para cada um dos meses que existir wc - A MLT varia no
%           tempo em fun��o das mudan�as de configura��es
%
% wc      - Coeficientes do PAR em matriz onde as colunas s�o cada um dos meses
%           e as linhas a ordem
%         - sempre deve come�ar em janeiro, mesmo que o primeiro m�s a ser
%           previsto seja diferente
%
% mes     - primeiro m�s a ser previsto


%%% Circular EAFPAST E MLT de acordo com a ordem do PAR e replicar se for
%%% para mais de um ano
ord = size(wc,1);
N   = size(wc,2);

eafpast = repmat(eafpast,1,ceil(N/12));

eafpast = [eafpast(:,12-ord+1:12) eafpast(:,1:N)];
mlt     = [mlt_conf1(12-ord+1:12) mlt];

%%% Retirar a MLT da EAFPAST
aux_pass = eafpast - repmat(mlt,size(eafpast,1),1);
pass3    = aux_pass(:,ord+1:end);

%%% Passar pelo PAR
aux_wc = fliplr(wc');
for ii = mes:size(aux_wc,1)
    pass3(:,ii) = sum(aux_pass(:,ii:ii+ord-1).*repmat(aux_wc(ii,:),size(eafpast,1),1),2);
    aux_pass(:,ii+ord) = pass3(:,ii);
end

%%% Recolocar a MLT
pass3 = pass3' + repmat(mlt(:,ord+1:end),size(eafpast,1),1)';


