function [ Tx_Desc, Tx_Desc_aux ] = taxa_desconto( N, tx_desconto, Ab, contC )
%TAXA_DESCONTO Retorna a taxa de desconto para os custos de todos os
%cenarios
%   Esta fun��o tem como entrada a taxa de desconto anual. Calcula-se a
%   taxa mensal, depois a taxa composta e esta � retornada pela fun��o

col = size(Ab,2);
auxN = [1:N];

%%% Leontina, o c�lculo da taxa de desconto resumem-se a estas duas linhas

% Passar do anual para o mensal
tx_desconto = ((1 + tx_desconto/100)^(1/12))-1;

% Calcular taxa composta para cada m�s
tx_desconto = (1/(1 + tx_desconto)).^(auxN-1);

tx_desconto = repmat(tx_desconto,col,1);
Tx_Desc = reshape(tx_desconto,[],1);
Tx_Desc_aux = Tx_Desc;
Tx_Desc_aux(1:contC) = [];


end
