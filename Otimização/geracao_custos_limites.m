function [ P, aux_posicao_Q, lim_inferior, lim_superior, custos_operacao, ...
  custos_expansao ] = geracao_custos_limites( A, contC, Q, Folga, V,...
  S,T, I,  N_submercado, N, Q_exp, p_GH_exp, V_exp, ...
  I_exp, p_I_exp,p_T_exp, T_exp,p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp)
  %GERACAO_CUSTOS_LIMITES Retorna os custos de opera??o e expans?o e os
  %limites superior e inferior das vari?veis
  %   Para todas as vari?veis do problema para 1 cen?rio, ele pega os seus
  %   valores m?ximos e m?nimos e os p?e nos limites superiores e inferiores
  %   do problema, respectivamente. Ele, tamb?m, coloca os custos de expans?o
  %   e opera??o de todas as vari?veis do problema para poder, futuramente,
  %   montar a fun??o objetivo.

  lim_inferior = zeros(size(A,2),1);
  lim_superior = zeros(size(A,2),1);
  custos_operacao = zeros(size(A,2),1);
  custos_expansao = zeros(size(A,2),1);

  P = contC.*([1:N]-1);
  aux_posicao_Q = [P+Q(1).p1 P+Q(2).p1 P+Q(3).p1 P+Q(4).p1];

  for sub = 1:N_submercado
    % Folga(sub).p1
    lim_inferior(P+Folga(sub).p1) = zeros(N,1);
    lim_superior(P+Folga(sub).p1) = Folga(sub).maximo;
    custos_operacao(P+Folga(sub).p1) = Folga(sub).custo;
    custos_expansao(P+Folga(sub).p1) = zeros(size(P));

    % V(sub).p1
    lim_inferior(P+V(sub).p1) = zeros(size(V(sub).minimo));
    lim_superior(P+V(sub).p1) = V(sub).maximo;
    custos_operacao(P+V(sub).p1) = zeros(size(P));
    custos_expansao(P+V(sub).p1) = zeros(size(P));

    % S(sub).p1
    lim_inferior(P+S(sub).p1) = S(sub).minimo;
    lim_superior(P+S(sub).p1) = S(sub).maximo;
    custos_operacao(P+S(sub).p1) = S(sub).custo;
    custos_expansao(P+S(sub).p1) = zeros(size(P));

    % Q(sub).p1
    %     lim_inferior(P+Q(sub).p1) = squeeze(energia_fio(sub,:,1));%Q(sub).minimo;
    lim_inferior(P+Q(sub).p1) = zeros(N,1);
    lim_superior(P+Q(sub).p1) = Q(sub).maximo;

    custos_operacao(P+Q(sub).p1) = Q(sub).custo;
    custos_expansao(P+Q(sub).p1) = zeros(size(P));


    % T(sub).p1  %super jogada
    aux_P = kron(P,ones(size(T(sub).p1)));
    aux_T = kron(ones(size(P)),T(sub).p1);
    aux_T = aux_T+aux_P;
    lim_inferior(aux_T) = reshape(T(sub).minimo',1,[]);
    lim_superior(aux_T) = reshape(T(sub).maximo',1,[]);
    custos_operacao(aux_T) = reshape(T(sub).custo',1,[]);
    custos_expansao(aux_T) = zeros(size(reshape(T(sub).custo',1,[])));

    % T_exp(sub).p1  %super jogada
    aux_P = kron(P,ones(size(T_exp(sub).p1)));
    aux_T_exp = kron(ones(size(P)),T_exp(sub).p1);
    aux_T_exp = aux_T_exp+aux_P;
    lim_inferior(aux_T_exp) = reshape(T_exp(sub).minimo',1,[]);
    lim_superior(aux_T_exp) = reshape(T_exp(sub).maximo',1,[]);
    custos_operacao(aux_T_exp) = reshape(T_exp(sub).custo',1,[]);
    custos_expansao(aux_T_exp) = zeros(size(reshape(T_exp(sub).custo',1,[])));

    % p_T_exp(sub).p1  %super jogada
    aux_P = kron(P,ones(size(p_T_exp(sub).p1)));
    aux_p_T_exp = kron(ones(size(P)),p_T_exp(sub).p1);
    aux_p_T_exp = aux_p_T_exp+aux_P;
    lim_inferior(aux_p_T_exp) = reshape(p_T_exp(sub).minimo',1,[]);
    lim_superior(aux_p_T_exp) = reshape(p_T_exp(sub).maximo',1,[]);
    custos_operacao(aux_p_T_exp) = zeros(size(reshape(T_exp(sub).custo',1,[])));
    custos_expansao(aux_p_T_exp) = reshape(p_T_exp(sub).custo',1,[]);
    
    % p_cons_T_exp(sub).p1  %super jogada
    aux_P = kron(P,ones(size(p_cons_T_exp(sub).p1)));
    aux_p_cons_T_exp = kron(ones(size(P)),p_cons_T_exp(sub).p1);
    aux_p_cons_T_exp = aux_p_cons_T_exp+aux_P;
    lim_inferior(aux_p_cons_T_exp) = reshape(p_cons_T_exp(sub).minimo',1,[]);
    lim_superior(aux_p_cons_T_exp) = reshape(p_cons_T_exp(sub).maximo',1,[]);
    custos_operacao(aux_p_cons_T_exp) = zeros(size(reshape(T_exp(sub).custo',1,[])));
    custos_expansao(aux_p_cons_T_exp) = reshape(p_cons_T_exp(sub).custo',1,[]);
    
    
    

    % Q_exp(sub).p1
    aux_P = kron(P,ones(size(Q_exp(sub).p1)));
    aux_Q_exp = kron(ones(size(P)),Q_exp(sub).p1);
    aux_Q_exp = aux_Q_exp+aux_P;
    lim_inferior(aux_Q_exp) = reshape(Q_exp(sub).minimo',1,[]);
    lim_superior(aux_Q_exp) = reshape(Q_exp(sub).maximo',1,[]);

    custos_operacao(aux_Q_exp) = reshape(Q_exp(sub).custo',1,[]);
    custos_expansao(aux_Q_exp) = zeros(size(reshape(Q_exp(sub).custo',1,[])));

    % p_GH_exp(sub).p1
    aux_P = kron(P,ones(size(p_GH_exp(sub).p1)));
    aux_GH_exp = kron(ones(size(P)),p_GH_exp(sub).p1);
    aux_GH_exp = aux_GH_exp+aux_P;
    lim_inferior(aux_GH_exp) = reshape(p_GH_exp(sub).minimo',1,[]);
    lim_superior(aux_GH_exp) = reshape(p_GH_exp(sub).maximo',1,[]);

    custos_operacao(aux_GH_exp) = zeros(size(reshape(p_GH_exp(sub).maximo',1,[])));
    custos_expansao(aux_GH_exp) = reshape(p_GH_exp(sub).custo',1,[]);

    % Colocando limite superior de p_GH_exp em 0 para os instantes de tempo em que
    % a usina nao vai completar sua construcao(exemplo: se a usina demora 2 anos
    % para ser construida, durante 2 anos nao se pode colocar p_GH_exp desta usina
    % maior que 0)
    for n=1:size(p_GH_exp(sub).p1,2)
      tempo_construcao = p_GH_exp(sub).tempo_construcao(n);
      P_construcao = contC.*([1:tempo_construcao]-1);
      lim_superior(P_construcao+p_GH_exp(sub).p1(n)) = 0;
    end
    
    
    % p_cons_GH_exp(sub).p1
    aux_P = kron(P,ones(size(p_cons_GH_exp(sub).p1)));
    aux_cons_GH_exp = kron(ones(size(P)),p_cons_GH_exp(sub).p1);
    aux_cons_GH_exp = aux_cons_GH_exp+aux_P;
    lim_inferior(aux_cons_GH_exp) = reshape(p_cons_GH_exp(sub).minimo',1,[]);
    lim_superior(aux_cons_GH_exp) = reshape(p_cons_GH_exp(sub).maximo',1,[]);

    custos_operacao(aux_cons_GH_exp) = zeros(size(reshape(p_GH_exp(sub).maximo',1,[])));
    custos_expansao(aux_cons_GH_exp) = reshape(p_cons_GH_exp(sub).custo',1,[]);
    
    % V_exp(sub).p1
    aux_P = kron(P,ones(size(V_exp(sub).p1)));
    aux_V_exp = kron(ones(size(P)),V_exp(sub).p1);
    aux_V_exp = aux_V_exp+aux_P;
    lim_inferior(aux_V_exp) = reshape(V_exp(sub).minimo',1,[]);
    lim_superior(aux_V_exp) = reshape(V_exp(sub).maximo',1,[]);

    custos_operacao(aux_V_exp) = zeros(size(reshape(V_exp(sub).minimo',1,[])));
    custos_expansao(aux_V_exp) = zeros(size(reshape(V_exp(sub).minimo',1,[])));

  end
  aux_P = kron(P,ones(size(I.p1)));
  aux_I = kron(ones(size(P)),I.p1);
  aux_I = aux_I+aux_P;
  lim_inferior(aux_I) = reshape(I.minimo',1,[]);
  lim_superior(aux_I) = reshape(I.maximo',1,[]);
  custos_operacao(aux_I) = reshape(I.custo',1,[]);
  custos_expansao(aux_I) = zeros(size(reshape(I.custo',1,[])));

  aux_P = kron(P,ones(size(I_exp.p1)));
  aux_I_exp = kron(ones(size(P)),I_exp.p1);
  aux_I_exp = aux_I_exp+aux_P;
  lim_inferior(aux_I_exp) = reshape(I_exp.minimo',1,[]);
  lim_superior(aux_I_exp) = reshape(I_exp.maximo',1,[]);
  custos_operacao(aux_I_exp) = reshape(I_exp.custo',1,[]);
  custos_expansao(aux_I_exp) = zeros(size(reshape(I_exp.custo',1,[])));

  aux_P = kron(P,ones(size(p_I_exp.p1)));
  aux_p_I_exp = kron(ones(size(P)),p_I_exp.p1);
  aux_p_I_exp = aux_p_I_exp+aux_P;
  lim_inferior(aux_p_I_exp) = reshape(p_I_exp.minimo',1,[]);
  lim_superior(aux_p_I_exp) = reshape(p_I_exp.maximo',1,[]);
  custos_operacao(aux_p_I_exp) = zeros(size(reshape(p_I_exp.custo',1,[])));
  custos_expansao(aux_p_I_exp) = reshape(p_I_exp.custo',1,[]);
  
  aux_P = kron(P,ones(size(p_cons_I_exp.p1)));
  aux_p_cons_I_exp = kron(ones(size(P)),p_cons_I_exp.p1);
  aux_p_cons_I_exp = aux_p_cons_I_exp+aux_P;
  lim_inferior(aux_p_cons_I_exp) = reshape(p_cons_I_exp.minimo',1,[]);
  lim_superior(aux_p_cons_I_exp) = reshape(p_cons_I_exp.maximo',1,[]);
  custos_operacao(aux_p_cons_I_exp) = zeros(size(reshape(p_cons_I_exp.custo',1,[])));
  custos_expansao(aux_p_cons_I_exp) = reshape(p_cons_I_exp.custo',1,[]);



end
