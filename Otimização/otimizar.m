function handles = otimizar(handles)
% OTIMIZAR Fun��o que faz a otimiza��o do problema de planejamento el�trico
%   Esta fun��o monta as matrizes com os dados do problema de otimiza��o e monta o
%   problema de otimiza��o e o resolV_exp.

tic

num_cenario = handles.num_cen;
prob_cenario = handles.prob_cen;
N = handles.N;
N_submercado = handles.N_sub;

tx_desconto = handles.tx_desc;

I = handles.I;
T = handles.T;
V = handles.V;
S = handles.S;
Q = handles.Q;
D = handles.D;
Afl = handles.A;
V_exp = handles.V_exp;
I_exp = handles.I_exp;
Q_exp = handles.Q_exp;
p_I_exp = handles.p_I_exp;
p_GH_exp = handles.p_GH_exp;
T_exp = handles.T_exp;
p_T_exp = handles.p_T_exp;
p_cons_GH_exp = handles.p_cons_GH_exp;
p_cons_I_exp = handles.p_cons_I_exp;
p_cons_T_exp = handles.p_cons_T_exp;

energia_fio = handles.EF;
Folga = handles.Folga;



% --------------------------- Replicar a Demanda --------------------------
D = repmat(D,[1 1 num_cenario]);

% ----------- Primeiro Cen�rio igual a m�dia e Curva de Infla��o ----------
inflacao   = [1 1 1 1];
N_meses = handles.edit_N_meses;
if N_meses > 12
    N_meses = 12;
end


for sub = 1:N_submercado
    Folga(sub).custo(:,:) = 999999;
    
    %%% Primeiro Cen�rio igual a m�dia
    Afl(sub,1,:) = mean(Afl(sub,1,:),3);
    energia_fio(sub,1,:)  = mean(energia_fio(sub,1,:),3);
    
    %%% Aplicar Multiplicadores
    Afl(sub,1:N_meses,:) = Afl(sub,1:N_meses,:).*repmat(inflacao(sub), ...
        [1 N_meses num_cenario]);
    energia_fio(sub,1:N_meses,:)  = ...
        energia_fio(sub,1:N_meses,:).*repmat(inflacao(sub),[1 N_meses num_cenario]);
end


%F  V  S  Q Q_exp p*GH_exp V_exp 
% Abase = Matriz do tempo t
% Abase_1 = Matriz do tempo t - 1
Abase   = [1  1  0  0;   % Restricao de Folga
    0  1  1  1;   % Restricao de Volume
    0  0  0  1;   % Restricao Energetica
    ];

Abase_1 = [0  0  0  0;
    0 -1  0  0;
    0  0  0  0;
    ];

% Gerando Matriz A para N = 1 e cenario = 1
[ RFolga, RVol, REnerg, RNo, R_Exp_Q, R_Exp_V, Q_exp, p_GH_exp, R_Exp_GH_T,...
  Ab, Ab_1, contC, contR, Folga, V, Q, T, V_exp, I_exp, p_I_exp, ...
  I, S, R_Exp_I_pos, R_Exp_I_neg, R_Exp_I_T, R_Exp_T, R_Exp_T_T,...
  T_exp, p_T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp, R_Exp_T_Cons,...
  R_Exp_GH_Cons, R_Exp_I_Cons] = montagem_matriz_cen_1( N_submercado, T, Folga,...
  S, V, Q, I, Abase, Abase_1,T_exp,I_exp,Q_exp, V_exp, p_I_exp, p_GH_exp,...
  p_cons_GH_exp, p_T_exp, p_cons_T_exp, p_cons_I_exp);


% ---------------------------- Cortar no Tempo ----------------------------

% D, energia_fio e Afl
D   = D(:,1:N,:);
energia_fio  = energia_fio(:,1:N,:);
Afl = Afl(:,1:N,:);

[ I, Folga, Q, S, V, p_GH_exp, T, Q_exp, V_exp, I_exp,p_I_exp ...
  , p_T_exp, T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp] = ...
  cortar_dados_no_tempo( I, Folga, Q, S, V, p_GH_exp, Q_exp,V_exp,...
  I_exp, p_I_exp,T,p_T_exp, T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp, N, N_submercado);


% ---------------------- Iniciar montagem da Matriz -----------------------
% Matriz identidade NxN(N = numero de per�odos de tempo)
matriz_identidade = eye(N);

matriz_identidade_1 = matriz_identidade;
matriz_identidade_1(end,:) = [];
matriz_identidade_1 = [zeros(1,size(matriz_identidade_1,2)); ...
                        matriz_identidade_1];

% Matriz A com N per�odos de tempo
A = kron(matriz_identidade,Ab) + kron(matriz_identidade_1,Ab_1);

% ---------------------- Limites Inferior e Superior ----------------------
[ P, aux_posicao_Q, lim_inferior, lim_superior, custos_operacao, ...
  custos_expansao ] = geracao_custos_limites( A, contC, Q, Folga, V,...
  S,T, I,  N_submercado, N, Q_exp, p_GH_exp, V_exp, ...
  I_exp, p_I_exp,p_T_exp, T_exp,p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp);

% ------------------- Ajeitar a matriz para os cen�rios -------------------

[ A_sparse ] = matriz_com_cenarios( A, Ab_1,num_cenario, contC,...
    contR);

% Liberando Mem�ria
A = sparse(A);


% --------------------------- Taxa de Desconto ----------------------------
[ Tx_Desc, Tx_Desc_aux ] = taxa_desconto( N, tx_desconto, Ab, contC );

% -------------------- Limites e custos por cen�rios ----------------------
[ BH, BL , LLS, LLI, custos_operacao, custos_expansao, Tx_Desc, R, auxR ] =...
  geracao_limites_custos_cenarios( num_cenario, contR, A, N_submercado,...
  I, V, RFolga, RVol, Afl, REnerg, D, RNo,...
  Q, custos_operacao, custos_expansao, lim_inferior,...
  Tx_Desc, N, prob_cenario, lim_superior,contC, energia_fio, aux_posicao_Q,...
  Tx_Desc_aux, T, R_Exp_Q, Q_exp, R_Exp_V, V_exp,R_Exp_GH_T, ...
  R_Exp_I_pos, R_Exp_I_neg, R_Exp_I_T, R_Exp_T, R_Exp_T_T, R_Exp_GH_Cons, ...
  R_Exp_I_Cons, R_Exp_T_Cons);

% ------------------------------ Matriz com restri��es que s�o inequa��es -


% ---------------------------Integer Constraints---------------------------
[ integer_constraints ] = geracao_indices_variaveis( contC, ...
    num_cenario, N_submercado, p_GH_exp, p_T_exp, p_I_exp, Ab, N, I);


% ------------------------------- Otimizar --------------------------------
tic

Prob.P = 1;

auxC = (custos_operacao + custos_expansao).*Tx_Desc;


% Retirando matrizes n�o mais necess�rias para a execu��o do software para
% liberar espa�o namem�ria
clear A Ab Ab_1 Abase Abase_1 custos_expansao custos_operacao indices_excluidos_A
clear indices_excluidos_b lim_inferior lim_superior matriz_identidade 
clear matriz_identidade_1 Tx_Desc Tx_Desc_aux

prob = SolverSDK('Problem');

% Localizando engine do gurobi
SolverSDK('EngineAdd', prob, 'Gurobi LP', [handles.pwd '\Gurobieng.dll']);

% Montando problema de otimiza??o
SolverSDK('ProbEngineSet', prob, 'Gurobi LP');
SolverSDK('VariableAdd', prob, 'Vars', 'Decision', size(A_sparse,2));
SolverSDK('FunctionAdd', prob, 'Constraints', 'Constraint', size(BH,1));
SolverSDK('FunctionAdd', prob, 'Objective', 'Objective', 1);

SolverSDK('VarUpperBoundSet', prob, 'Vars', LLS');
SolverSDK('VarLowerBoundSet', prob, 'Vars', LLI');

SolverSDK('ModAllLinearSet', prob, 'Objective', auxC');
SolverSDK('ModAllLinearSet', prob, 'Constraint', (A_sparse));


SolverSDK('FcnUpperBoundSet', prob, 'Constraints', (BH));
SolverSDK('FcnLowerBoundSet', prob, 'Constraints', (BL));

[status, f_k] = SolverSDK('SolverOptimize', prob);
 [x] = SolverSDK('VarFinalValue', prob, 'Vars');
lambda = SolverSDK('FcnDualValue', prob, 'Constraints');

% output
% Limitando valores de x a duas casas decimais
x = round(x/0.01)*0.01;
x = x';
pi_r = lambda';
horas_mes = 730.5; 
fVal     = f_k*horas_mes;



if status==0
    disp(num2str(toc,'Otimizado em: %.1f segundos'))
else
    disp('N�o Otimizado')
    disp(num2str(toc,'N�o Otimizado em: %.1f segundos'))
end

% ------------------------- Guardar os Resultados -------------------------


for ii=1:N_submercado
    resp(ii).T=zeros(N,T(sub).numero,num_cenario);
    resp(ii).T_exp=zeros(N,T_exp(sub).numero,num_cenario);
end
for cen=1:num_cenario
    if cen==1
        PI_R=pi_r(1:contR*N);
        X=x(1:contC*N);
        x(1:contC*N)=[];
        pi_r(1:contR*N)=[];
    else
        X=[X(1:contC); x(1:contC*(N-1))];
        PI_R=[PI_R(1:contR); pi_r(1:contR*(N-1))];
        x(1:contC*(N-1))=[];
        pi_r(1:contR*(N-1))=[];
    end
    %P e R
    for sub=1:N_submercado
        % Turbinamentos de expans�o
        aux_P = kron(P,ones(size(Q_exp(sub).p1)));
        aux_Q_exp = kron(ones(size(P)),Q_exp(sub).p1);
        aux_Q_exp = aux_Q_exp+aux_P;
        
        % Armazenamentos de expans�o
        aux_P = kron(P,ones(size(V_exp(sub).p1)));
        aux_V_exp = kron(ones(size(P)),V_exp(sub).p1);
        aux_V_exp = aux_V_exp+aux_P;
        
        % Expans�o de hidr�ulica
        aux_P = kron(P,ones(size(p_GH_exp(sub).p1)));
        aux_GH_exp = kron(ones(size(P)),p_GH_exp(sub).p1);
        aux_GH_exp = aux_GH_exp+aux_P;
        
        
        resp(sub).F(1:N,cen)=squeeze(X([P+Folga(sub).p1]));
        resp(sub).V(1:N,cen)=squeeze(X([P+V(sub).p1])) + sum(reshape(squeeze(X(aux_V_exp)),V_exp(sub).numero,N)',2);
        resp(sub).S(1:N,cen)=squeeze(X([P+S(sub).p1]));
        % levando em conta a energia a fio
        resp(sub).Q(1:N,cen)=squeeze(X([P+Q(sub).p1])) + sum(reshape(squeeze(X([aux_Q_exp])),Q_exp(sub).numero,N)',2);
        %+squeeze(energia_fio(sub,1:N,cen))';
        
        resp(sub).Q_exp(1:N,cen) = sum(reshape(...
            squeeze(X([aux_GH_exp])),Q_exp(sub).numero,N)'.*Q_exp(sub).maximo,2);
        resp(sub).V_exp(1:N,cen) = sum(reshape(...
            squeeze(X([aux_GH_exp])),V_exp(sub).numero,N)'.*V_exp(sub).maximo,2);
        
        % T�rmicas
        aux_P=kron(P,ones(size(T(sub).p1)));
        aux_T=kron(ones(size(P)),T(sub).p1);
        aux_T=aux_T+aux_P;
        aux2T=X(aux_T);
        resp(sub).T(1:N,1:T(sub).numero,cen)=reshape(aux2T,T(sub).numero,N)';
        
        % T�rmicas de expans�o
        aux_P = kron(P,ones(size(T_exp(sub).p1)));
        aux_T_exp = kron(ones(size(P)),T_exp(sub).p1);
        aux_T_exp=aux_T_exp+aux_P;
        aux2T_exp = X(aux_T_exp);
        resp(sub).T_exp(1:N,1:T_exp(sub).numero,cen)=reshape(aux2T_exp,T_exp(sub).numero,N)';
        
        aux_P = kron(P,ones(size(p_T_exp(sub).p1)));
        aux_p_T_exp = kron(ones(size(P)),p_T_exp(sub).p1);
        aux_p_T_exp=aux_p_T_exp+aux_P;
        aux2p_T_exp = X(aux_p_T_exp);
        resp(sub).p_T_exp(1:N,cen)=sum(reshape(aux2p_T_exp,T_exp(sub).numero,N)'.*T_exp(sub).maximo,2);
    end
    %corrigir os fluxos do loop
    aux_P=kron(P,ones(size(I.p1)));
    aux_I=kron(ones(size(P)),I.p1);
    aux_I=aux_I+aux_P;
    aux2I=X(aux_I);
    
    aux_P = kron(P,ones(size(I_exp.p1)));
    aux_I_exp = kron(ones(size(P)),I_exp.p1);
    aux_I_exp = aux_I_exp + aux_P;
    aux2I_exp = X(aux_I_exp);
    
    aux_P = kron(P,ones(size(p_I_exp.p1)));
    aux_p_I_exp = kron(ones(size(P)),p_I_exp.p1);
    aux_p_I_exp = aux_p_I_exp + aux_P;
    aux2p_I_exp = X(aux_p_I_exp);

    F(cen).F(1:N,:)=reshape(aux2I,size(I.linhas,2),N)' + ...
        reshape(aux2I_exp, size(I.linhas,2),N)';
    F_exp(cen).F_exp(1:N,:) = reshape(aux2p_I_exp, size(I.linhas,2),N)'.*I_exp.maximo;
    
    menor=min(abs(F(cen).F(:,[2 3 4]))'); %entre as linhas do loop
    %Vou corrigir os Fluxos do Loop
    %     flag=[0 1 -1 1 0];
    %         for ii=1:size(menor,2)
    %             for iii=2:4
    %                 if F(cen).F(ii,iii)<0
    %                     F(cen).F(ii,iii)=flag(iii)*(F(cen).F(ii,iii)+menor(ii));
    %                 else
    %                     F(cen).F(ii,iii)=flag(iii)*(F(cen).F(ii,iii)-menor(ii));
    %                 end
    %             end
    %         end
    
    % Restricao de Volume RVol(sub).p1 [Afl Afl]
    aux_V=kron(ones(1,length(R)),[RVol(:).p1]);
    aux_V=aux_V+auxR;
    aux2V=PI_R(aux_V);
    
    % Restricao Energetica REnerg(sub).p1 [D-energia_fio D-energia_fio]
    auxEnerg=kron(ones(1,length(R)),[REnerg(:).p1]);
    auxEnerg=auxEnerg+auxR;
    aux2Energ=PI_R(auxEnerg);
    
    aux=reshape(aux2V,N_submercado,N)';
    cmo(cen).A=aux;
    aux=-reshape(aux2Energ,N_submercado,N)';
    cmo(cen).D=-aux;
end

%para os custos         %para o V_exptor b
%     V(sub).pont       %     p_D
%     S(sub).pont       %     p_A
%     Q(sub).pont       %     p_I
%     T(sub).pont
%     p_f

handles.T=T;
handles.V=V;
handles.S=S;
handles.Q=Q;
handles.T_exp = T_exp;
handles.p_T_exp = p_T_exp;

%handles.X=X;
% handles.pi_ida=pi_ida;
handles.resp=resp;
handles.F=F;
handles.F_exp = F_exp;
handles.V_inicial=[V.inicial];





% handles.cmo=cmo;
% cmo.A = [zeros(100000,10)];
% cmo.D = [zeros(100000,10)];
handles.cmo = cmo;
handles.custo = fVal;
handles.flag_trab=1;
