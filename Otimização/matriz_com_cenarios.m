function [ A_sparse ] = matriz_com_cenarios( A, Ab_1,num_cenario, contC,...
    contR)
%MATRIZ_COM_CENARIOS Constr�i a matriz A considerando cen�rios
%   Esta fun��o pega a matriz A para 1 cen�rio e cria a matriz A para todos
%   os num_cenarios cen�rios do problema a ser resolvido. No final, ela
%   retorna uma matriz esparsa de A para os num_cenarios cen�rios.

A_aux = A;
A_aux(:,1:contC) = [];
A_aux(1:contR,:) = [];

A_sparse = sparse(A);

%%% Preparar o conector de tempo pra ao trabalho
Ab_1 = [Ab_1; zeros( size(A_aux,1)-size(Ab_1,1),size(Ab_1,2))];

%%% Colocar o restante do tempo
Ab_1 = [Ab_1 zeros( size(Ab_1,1),size(A_sparse,2)-size(Ab_1,2))];

Ab_1 = sparse(Ab_1);

Zaux_sparse = sparse(size(A_aux,1),size(A_aux,2));
ZA_sparse   = sparse(size(A,1),size(A_aux,2));

for cen = 2:num_cenario
    A_sparse_aux = [Ab_1 repmat(Zaux_sparse,1,cen-2) sparse(A_aux) ];
    if cen==2
        A_sparse = [A_sparse ZA_sparse];
    else
        A_sparse = [A_sparse [ ZA_sparse; repmat(Zaux_sparse,cen-2,1)]];
    end
    A_sparse = [A_sparse; A_sparse_aux];
end
end

