function [ BH, BL , LLS, LLI, custos_operacao, custos_expansao, Tx_Desc, R, auxR ] =...
  geracao_limites_custos_cenarios( num_cenario, contR, A, N_submercado,...
  I, V, RFolga, RVol, Afl, REnerg, D, RNo,...
  Q, custos_operacao, custos_expansao, lim_inferior,...
  Tx_Desc, N, prob_cenario, lim_superior,contC, energia_fio, aux_posicao_Q,...
  Tx_Desc_aux, T, R_Exp_Q, Q_exp, R_Exp_V, V_exp,R_Exp_GH_T, ...
  R_Exp_I_pos, R_Exp_I_neg, R_Exp_I_T, R_Exp_T, R_Exp_T_T, R_Exp_GH_Cons, ...
  R_Exp_I_Cons, R_Exp_T_Cons)
  %GERACAO_LIMITES_CUSTOS_CENARIOS Fun??o que gera os limites superiores e
  % inferiores, custos de expans?o e de opera??o para cada cen?rio. Tamb?m
  % gera a matriz B.
  % Esta fun??o, baseada nos limites inferiores e superiores para um cen?rio,
  % gera os limites superiores e inferiores para cada cen?rio. Ela, tamb?m,
  % faz o mesmo para os custos de expans?o e de opera??o. Por fim, ela pega
  % os valores de aflu?ncia, demanda, volume, turbinamento, entre outros,
  % para gerar a matriz B.


  % --------------------- Auxiliares de Limites e Custo ---------------------
  lim_inferior_aux = lim_inferior;
  lim_superior_aux = lim_superior;
  custos_operacao_aux = custos_operacao;
  custos_expansao_aux = custos_expansao;

  %%% Retira o primeiro instante de tempo
  lim_inferior_aux(1:contC) = [];
  lim_superior_aux(1:contC) = [];
  custos_operacao_aux(1:contC) = [];
  custos_expansao_aux(1:contC) = [];

  custos_operacao(contC+1:end) = custos_operacao(contC+1:end)*prob_cenario(1);
  custos_expansao(contC+1:end) = custos_expansao(contC+1:end)*prob_cenario(1);
  LLI = lim_inferior;
  LLS = lim_superior;
  BH= [];
  BL= [];
  
  % -------------------- Limites e custos por cen�rios ----------------------

  for cen = 1:num_cenario


    R = contR.*([1:N]-1);
    bh = zeros(size(A,1),1);
    bl = zeros(size(A,1),1);
    
    auxR = kron(R,ones(1,N_submercado));
    aux_constraints_I = kron(R,ones(1,size(I.linhas,1)));


    % Restricao de Folga
    auxF = kron(ones(1,length(R)),[RFolga(:).p1]) + auxR;
    bl(auxF) = -Inf;
    bh(auxF) = -1*reshape([V(:).minimo]', [], 1);

    % Balanco de Afluencia
    aux_V = kron(ones(1,length(R)),[RVol(:).p1]) + auxR; 
    bh(aux_V) = reshape(squeeze(Afl(:,:, cen)),[],1);
    bh([RVol(:).p1]) = bh([RVol(:).p1])+[V(:).inicial]';
    bl(aux_V) = reshape(squeeze(Afl(:,:, cen)),[],1);
    bl([RVol(:).p1]) = bl([RVol(:).p1])+[V(:).inicial]';
    
    % Balanco Energetico
    auxEnerg = kron(ones(1,length(R)),[REnerg(:).p1]) + auxR;
    bh(auxEnerg) = reshape(squeeze(D(:,:,cen)),[],1);
    bl(auxEnerg) = reshape(squeeze(D(:,:,cen)),[],1);
    
    % restricao de No
    auxNo = kron(ones(1,N),RNo.p1)+kron(R,ones(1,length(RNo.p1)));
    bh(auxNo) = 0;
    bl(auxNo) = 0;

    % Pegando a quantidade de constraints de Q_exp, p_GH_exp e V_exp
    % por cenario e tempo.
    length_Q_exp = 0;
    length_GH_exp_T = 0;
    length_V_exp = 0;
    length_GH_exp_Cons = 0;

    for sub = 1:N_submercado
      length_Q_exp = length_Q_exp + length(R_Exp_Q(sub).p1);
      length_GH_exp_T = length_GH_exp_T + length(R_Exp_GH_T(sub).p1);
      length_V_exp = length_V_exp + length(R_Exp_V(sub).p1);
      length_GH_exp_Cons = length_GH_exp_Cons + length(R_Exp_GH_Cons(sub).p1);
    end

    % Restricao de expansao de turbinamento
    auxExpQ = kron(ones(1,N),[R_Exp_Q(:).p1]) + kron(R,ones(1,length_Q_exp));
    bh(auxExpQ) = 0;
    bl(auxExpQ) = -Inf;
    
    auxExpGHT = kron(ones(1,N),[R_Exp_GH_T(:).p1]) + kron(R,ones(1,length_GH_exp_T));
    bh(auxExpGHT) = 0;
    bl(auxExpGHT) = -Inf;
    
    auxExpGHCons = kron(ones(1,N),[R_Exp_GH_Cons(:).p1]) + kron(R,ones(1,length_GH_exp_Cons));
    bh(auxExpGHCons) = 0;
    bl(auxExpGHCons) = 0;
    
    % Restri��o de expans�o de turbinamento
    auxExpV = kron(ones(1,N),[R_Exp_V(:).p1]) + kron(R,ones(1,length_V_exp));
    bh(auxExpV) = 0;
    bl(auxExpV) = -Inf;

    % restricao de expans�o de interc�mbio
    auxExpIPos = kron(ones(1,N),[R_Exp_I_pos.p1]) + aux_constraints_I;
    bh(auxExpIPos) = 0;
    bl(auxExpIPos) = -Inf;

    auxExpINeg = kron(ones(1,N),[R_Exp_I_neg.p1]) + aux_constraints_I;
    bh(auxExpINeg) = 0;
    bl(auxExpINeg) = -Inf;

    auxExpIT = kron(ones(1,N),[R_Exp_I_T.p1]) + aux_constraints_I;
    bh(auxExpIT) = 0;
    bl(auxExpIT) = -Inf;
    
    auxExpICons = kron(ones(1,N),[R_Exp_I_Cons.p1]) + aux_constraints_I;
    bh(auxExpICons) = 0;
    bl(auxExpICons) = 0;

    %%%%%% Restri??o de T?rmicas

    % Pegando a quantidade de constraints de T_exp por cen?rio e tempo.
    length_T_exp = 0;
    length_T_exp_T = 0;
    length_T_exp_Cons = 0;

    for sub = 1:N_submercado
      length_T_exp = length_T_exp + length(R_Exp_T(sub).p1);
      length_T_exp_T = length_T_exp_T + length(R_Exp_T_T(sub).p1);
      length_T_exp_Cons = length_T_exp_Cons + length(R_Exp_T_Cons(sub).p1);
    end
    auxT_exp = kron(ones(1,N),[R_Exp_T(:).p1]) + kron(R,ones(1,length_T_exp));
    bh(auxT_exp) = 0;
    bl(auxT_exp) = -Inf;
    
    aux_T_exp_T = kron(ones(1,N),[R_Exp_T_T(:).p1]) + kron(R,ones(1,length_T_exp_T));
    bh(aux_T_exp_T) = 0;
    bl(aux_T_exp_T) = -Inf;
    
    aux_T_exp_Cons = kron(ones(1,N),[R_Exp_T_Cons(:).p1]) + kron(R,ones(1,length_T_exp_Cons));
    bh(aux_T_exp_Cons) = 0;
    bl(aux_T_exp_Cons) = 0;
    
    if cen>1
      bh(1:contR) = [];
      bl(1:contR) = [];
      BH = [BH; bh];
      BL = [BL; bl];
      
      %%% Recolocar os limites inferiores da geracao
      lim_inferior_aux = lim_inferior;
%       lim_inferior_aux(aux_posicao_Q) = [squeeze(energia_fio(1,:,cen))...
%       squeeze(energia_fio(2,:,cen))...
%       squeeze(energia_fio(3,:,cen)) squeeze(energia_fio(4,:,cen))];
      lim_inferior_aux(1:contC) = [];

      % Adicionando limites inferiores e superiores, custos de opera??o e
      % expans?o do cen?rio "cen" as listas de todos os limites inferiores
      % e superiores, custo de opera??o e expans?o.
      LLI = [LLI; lim_inferior_aux];
      LLS = [LLS; lim_superior_aux];
      custos_operacao = [custos_operacao; custos_operacao_aux*prob_cenario(cen)];
      custos_expansao = [custos_expansao; custos_expansao_aux*prob_cenario(cen)];
      Tx_Desc = [Tx_Desc; Tx_Desc_aux];
    else
      BH = [BH; bh];
      BL = [BL; bl];
    end
  end
end
