function [ RFolga, RVol, REnerg, RNo, R_Exp_Q, R_Exp_V, Q_exp, p_GH_exp, R_Exp_GH_T,...
  Ab, Ab_1, contC, contR, Folga, V, Q, T, V_exp, I_exp, p_I_exp, ...
  I, S, R_Exp_I_pos, R_Exp_I_neg, R_Exp_I_T, R_Exp_T, R_Exp_T_T,...
  T_exp, p_T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp, R_Exp_T_Cons,...
  R_Exp_GH_Cons, R_Exp_I_Cons] = montagem_matriz_cen_1( N_submercado, T, Folga,...
  S, V, Q, I, Abase, Abase_1,T_exp,I_exp,Q_exp, V_exp, p_I_exp, p_GH_exp,...
  p_cons_GH_exp, p_T_exp, p_cons_T_exp, p_cons_I_exp)
  %UNTITLED Summary of this function goes here
  %   Detailed explanation goes here

  %lista de restricoes
  RFolga = [];
  RVol = [];
  REnerg = [];
  RNo = [];
  R_Exp_Q = [];
  R_Exp_V = [];
  R_Exp_I_pos = [];
  R_Exp_I_neg = [];
  R_Exp_I_T = [];
  R_Exp_GH_T = [];
  R_Exp_T = [];
  R_Exp_T_T = [];
  R_Exp_T_Cons = [];
  R_Exp_GH_Cons = [];
  R_Exp_I_Cons = [];

  %Submercados
  Ab = [];
  Ab_1 = [];
  contC = 0;
  contR = 0;



  for sub = 1:N_submercado
    num_termicas = T(sub).numero;
    num_termicas_expansao = T_exp(sub).numero;

    %%% Ponteiros para as Vari?V_expis
    Folga(sub).p1 = 1+contC;
    V(sub).p1     = 2+contC;
    S(sub).p1     = 3+contC;
    Q(sub).p1     = 4+contC;
    T(sub).p1     = 4 + contC + [1:num_termicas];
    Q_exp(sub).p1 = 4 + contC + num_termicas + [1:Q_exp(sub).numero];
    p_GH_exp(sub).p1 = 4 + contC + num_termicas + Q_exp(sub).numero + [1:Q_exp(sub).numero];
    V_exp(sub).p1 = 4 + contC + num_termicas + 2*Q_exp(sub).numero + [1:Q_exp(sub).numero];
    p_cons_GH_exp(sub).p1 = 4 + contC + num_termicas + 3*Q_exp(sub).numero + [1:V_exp(sub).numero];
    T_exp(sub).p1 = 4 + contC + 3*Q_exp(sub).numero + V_exp(sub).numero +...
    num_termicas + [1:num_termicas_expansao];
    p_T_exp(sub).p1 = 4 + contC + 3*Q_exp(sub).numero + V_exp(sub).numero + ...
    num_termicas + num_termicas_expansao + [1:num_termicas_expansao];
    p_cons_T_exp(sub).p1 = 4 + contC + 3*Q_exp(sub).numero + V_exp(sub).numero + ...
    num_termicas + 2*num_termicas_expansao + [1:num_termicas_expansao];


    %%% Ponteiros para as restri??es
    RFolga(sub).p1 = 1+contR;
    RVol(sub).p1   = 2+contR;
    REnerg(sub).p1 = 3+contR;
    R_Exp_Q(sub).p1 = 3 + contR + [1:Q_exp(sub).numero] ;
    R_Exp_GH_T(sub).p1 = 3 + contR + Q_exp(sub).numero + [1:Q_exp(sub).numero];
    R_Exp_V(sub).p1 = 3 + contR + 2*Q_exp(sub).numero + [1:Q_exp(sub).numero];
    R_Exp_GH_Cons(sub).p1 = 3 + contR + 3*Q_exp(sub).numero + [1:V_exp(sub).numero];
    R_Exp_T(sub).p1 = 3 + contR + 3*Q_exp(sub).numero + V_exp(sub).numero +...
    [1:num_termicas_expansao];
    R_Exp_T_T(sub).p1 = 3 + contR + 3*Q_exp(sub).numero + V_exp(sub).numero +...
    num_termicas_expansao + [1:num_termicas_expansao];
    R_Exp_T_Cons(sub).p1 = 3 + contR + 3*Q_exp(sub).numero + V_exp(sub).numero +...
    2*num_termicas_expansao + [1:num_termicas_expansao];

    Q_exp_restricoes = [zeros(Q_exp(sub).numero, num_termicas) ...
        eye(Q_exp(sub).numero) -1*diag(Q_exp(sub).maximo(:,1)) ...
    zeros(Q_exp(sub).numero, 2*V_exp(sub).numero)];
    GH_exp_restricoes = [zeros(Q_exp(sub).numero, Q_exp(sub).numero + num_termicas)...
        -1*eye(Q_exp(sub).numero) zeros(Q_exp(sub).numero,2*V_exp(sub).numero)];
    V_exp_restricoes = [zeros(V_exp(sub).numero,Q_exp(sub).numero + num_termicas)...
        -1*diag(V_exp(sub).maximo(:,1)) eye(V_exp(sub).numero) ...
         zeros(V_exp(sub).numero, Q_exp(sub).numero)];
    GH_cons_restricoes = [zeros( Q_exp(sub).numero,num_termicas + ...
    Q_exp(sub).numero) -1*eye(Q_exp(sub).numero)...
    zeros(V_exp(sub).numero, Q_exp(sub).numero) eye(Q_exp(sub).numero)];

    num_GH_exp = size(Q_exp_restricoes,1) + size(V_exp_restricoes,1) +...
        size(GH_exp_restricoes,1) + size(GH_cons_restricoes,1);

    aux_Ab = [[Abase; zeros(3*num_termicas_expansao + 3*Q_exp(sub).numero + V_exp(sub).numero...
    ,size(Abase,2))] ...
    [[zeros(1,num_termicas + 2*Q_exp(sub).numero) ones(1,V_exp(sub).numero) zeros(1,V_exp(sub).numero)];
    [zeros(1,num_termicas) ones(1,Q_exp(sub).numero) zeros(1,Q_exp(sub).numero)...
    ones(1,V_exp(sub).numero) zeros(1,V_exp(sub).numero)];
    [ones(1,num_termicas + Q_exp(sub).numero) zeros(1,Q_exp(sub).numero + 2*V_exp(sub).numero)];
    [Q_exp_restricoes];
    [GH_exp_restricoes];
    [V_exp_restricoes];
    [GH_cons_restricoes];
    [zeros(3*num_termicas_expansao,num_termicas+ 2*Q_exp(sub).numero + 2*V_exp(sub).numero)]
    ]];

    size_Ab = size(Ab,1);

    % Adicionando a matriz A do submercado "sub" a matriz A do problema
    Ab   = [[Ab; zeros(size(aux_Ab,1),size(Ab,2))]...
    [zeros(size(Ab,1) , size(aux_Ab,2)); aux_Ab ]];

    % Gerando matriz A do submercado "sub" para o instante t-1

    Q_exp_restricoes_1 = zeros( Q_exp(sub).numero,num_termicas + ...
    3*Q_exp(sub).numero + V_exp(sub).numero);
    GH_exp_restricoes_1 = [[zeros(Q_exp(sub).numero,num_termicas + Q_exp(sub).numero)] ...
    [eye(Q_exp(sub).numero)] [zeros(Q_exp(sub).numero, 2*V_exp(sub).numero)]];
    V_exp_restricoes_1 = [zeros(V_exp(sub).numero,num_termicas + ...
    3*Q_exp(sub).numero + V_exp(sub).numero)];
    GH_cons_restricoes_1 = [zeros( Q_exp(sub).numero,num_termicas + ...
    Q_exp(sub).numero) eye(Q_exp(sub).numero) zeros(Q_exp(sub).numero, 2*V_exp(sub).numero)];

    aux_Ab_1 = [[Abase_1; zeros(3*num_termicas_expansao +2*Q_exp(sub).numero + 2*V_exp(sub).numero...
    , size(Abase_1,2))]...
    [
    [zeros(1,num_termicas + 2*Q_exp(sub).numero + 2*V_exp(sub).numero)]
    [zeros(1,num_termicas + 2*Q_exp(sub).numero) -1*ones(1,V_exp(sub).numero ) zeros(1,Q_exp(sub).numero) ];
    [zeros(size(Abase_1,1) - 2,num_termicas + 2*Q_exp(sub).numero + 2*V_exp(sub).numero)];
    [Q_exp_restricoes_1];
    [GH_exp_restricoes_1];
    [V_exp_restricoes_1];
    [GH_cons_restricoes_1];
    [zeros(3*num_termicas_expansao,num_termicas+ 2*Q_exp(sub).numero + 2*V_exp(sub).numero)]
    ];...
    ];

    % Adicionando a matriz A do submercado "sub" a matriz A do problema
    % para o instante t-1
    Ab_1 = [[Ab_1; zeros(size(aux_Ab_1,1),size(Ab_1,2))]...
    [zeros(size(Ab_1,1) , size(aux_Ab,2)); aux_Ab_1 ]];


    % Adicionando restri��o de expans�o de t�rmicas

    aux_T_exp = [zeros(2,3*num_termicas_expansao); [ones(1, num_termicas_expansao) zeros(1, 2*num_termicas_expansao)];
    zeros(num_GH_exp,3*num_termicas_expansao);
    [eye(num_termicas_expansao) diag(T_exp(sub).maximo(1,:))*-1 zeros(num_termicas_expansao)];
    [zeros(num_termicas_expansao) -1*eye(num_termicas_expansao) zeros(num_termicas_expansao)];
    [zeros(num_termicas_expansao) -1*eye(num_termicas_expansao) eye(num_termicas_expansao)]
    ];
    aux_T_exp_1 = [...
    zeros(size(Abase_1,1)+ num_GH_exp + num_termicas_expansao,3*num_termicas_expansao);...
    [zeros(num_termicas_expansao) eye(num_termicas_expansao) zeros(num_termicas_expansao)];
    [zeros(num_termicas_expansao) eye(num_termicas_expansao) zeros(num_termicas_expansao)]];

    Ab = [Ab [zeros(size_Ab, size(aux_T_exp,2)); aux_T_exp]];

    Ab_1 = [Ab_1 [zeros(size_Ab, size(aux_T_exp_1,2)); aux_T_exp_1]];
    
    %Atualizando contadores de restri??o e vari?veis.

    contC = 4 + contC + 3*Q_exp(sub).numero + V_exp(sub).numero + ...
    num_termicas + 3*num_termicas_expansao;
    contR = 3 + contR + 3*Q_exp(sub).numero + V_exp(sub).numero + ...
    3*num_termicas_expansao;

    if sub==N_submercado   %para os interc?mbios

      % Linhas de Interc?mbio
      I_linhas = I.linhas;
      I.p1 = contC+[1:size(I_linhas,2)];
      I_exp.p1 = contC + [size(I.linhas,2) + 1: 2*size(I.linhas,2)];

      % Construindo matriz que contem as equa??es de restri??o para
      % limites de interc?mbio.
      aux_I = zeros(size(Ab,1),2*size(I_linhas,2));
      aux_I([REnerg(:).p1]',:) = [I_linhas(1:end-I.N_nos,:) I_linhas(1:end-I.N_nos,:)];
      aux_I = [aux_I; [I_linhas(end-I.N_nos+1:end,:) I_linhas(end-I.N_nos+1:end,:)]];

      % Atualizando contador de var?veis
      contC = contC + 2*size(I_linhas,2);

      % Adicionando equa??es de restri??o a matriz A nos instantes t e t-1
      Ab = [[Ab; zeros(I.N_nos,size(Ab,2))]...
      [aux_I ]];

      Ab_1 = [[Ab_1; zeros(I.N_nos,size(Ab_1,2))]...
      [zeros(size(aux_I)) ]];

      % Expans?o de Linhas de Interc�mbio
      p_I_exp.p1 = contC + [1:size(I.linhas,2)];
      p_cons_I_exp.p1 = contC + size(I.linhas,2)+ [1:size(I.linhas,2)];


      % Construindo matriz que contem as equa�?es de restri�?o para
      % a expans?o do limites de interc�mbio.
      aux_I_Exp = zeros(size(Ab,1), size(I_linhas,2));
      aux_I_Exp = [aux_I_Exp; diag(I_exp.maximo(1,:))*-1;...
      diag(I_exp.maximo(1,:))*-1;...
      ];
      aux_I_Exp = [aux_I_Exp zeros(size(aux_I_Exp, 1), size(I_linhas,2))];

      % Atualizando contador de var�veis
      contC = contC + 2*size(I_linhas,2);

      restricoes_I_exp_positivo = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_exp_positivo(:, I_exp.p1) = eye(size(I_exp.p1,2));

      restricoes_I_exp_negativo = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_exp_negativo(:, I_exp.p1) = -1*eye(size(I_exp.p1,2));
      % Adicionando equa�?es de restri�?o a matriz A nos instantes t e
      % t-1
      Ab = [[Ab; restricoes_I_exp_positivo; restricoes_I_exp_negativo ]...
          aux_I_Exp ];

      Ab_1 = [[Ab_1; zeros(2*size(I_linhas,1),size(Ab_1,2))]...
          zeros(size(aux_I_Exp))];

      % Adicionando equa��es de restri��o: pt - pt-1 >= 0
      restricoes_I_exp = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_exp(:, p_I_exp.p1) = -1*eye(size(p_I_exp.p1,2));

      restricoes_I_exp_1 = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_exp_1(:, p_I_exp.p1) = eye(size(p_I_exp.p1,2));
      
      restricoes_I_cons = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_cons(:, p_cons_I_exp.p1) = eye(size(p_cons_I_exp.p1,2));
      restricoes_I_cons(:, p_I_exp.p1) = -1*eye(size(p_I_exp.p1,2));
      
      restricoes_I_cons_1 = zeros(size(I_linhas,1), size(Ab,2));
      restricoes_I_cons_1(:, p_I_exp.p1) = eye(size(p_I_exp.p1,2));

      Ab = [Ab; restricoes_I_exp; restricoes_I_cons];
      Ab_1 = [Ab_1; restricoes_I_exp_1; restricoes_I_cons_1];


      RNo.p1 = 1+contR;
      R_Exp_I_pos.p1 = [2:6] + contR;
      R_Exp_I_neg.p1 = [7:11] + contR;
      R_Exp_I_T.p1 = [12:16] + contR;
      R_Exp_I_Cons.p1 = [17:21] + contR;
      contR = contR+21;

    end
  end



end
