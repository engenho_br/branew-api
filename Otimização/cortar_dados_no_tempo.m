function [ I, Folga, Q, S, V, p_GH_exp, T, Q_exp, V_exp, I_exp,p_I_exp ...
  , p_T_exp, T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp] = ...
  cortar_dados_no_tempo( I, Folga, Q, S, V, p_GH_exp, Q_exp,V_exp,...
  I_exp, p_I_exp,T,p_T_exp, T_exp, p_cons_GH_exp, p_cons_I_exp, p_cons_T_exp, N, N_submercado)
  %CORTAR_DADOS_NO_TEMPO Fun??o que retorna as informa??es das vari?veis para
  %os N primeiros tempos.
  %   Esta fun??o, para todas as vari?veis do problema, garante que todas as
  %   vari?veis s? tenham informa??o sobre os N per?odos de tempo
  %   contemplados pelo problema de otimiza??o a ser resolvido.



  %   I
  I.maximo = I.maximo(1:N,:);
  I.minimo = I.minimo(1:N,:);
  % I.custo  = I.custo(1:N,:)+0.1;
  I.custo = I.custo(1:N,:) + 0.1;
  %
  I_exp.maximo = I_exp.maximo(1:N,:);
  I_exp.minimo = I_exp.minimo(1:N,:);
  % I.custo  = I.custo(1:N,:)+0.1;
  I_exp.custo = I.custo(1:N,:)+0.1;

  p_I_exp.maximo = p_I_exp.maximo(1:N,:);
  p_I_exp.minimo = p_I_exp.minimo(1:N,:);
  % I.custo  = I.custo(1:N,:)+0.1;
  p_I_exp.custo = p_I_exp.custo(1:N,:);
  
  p_cons_I_exp.maximo = p_cons_I_exp.maximo(1:N,:);
  p_cons_I_exp.minimo = p_cons_I_exp.minimo(1:N,:);
  % I.custo  = I.custo(1:N,:)+0.1;
  p_cons_I_exp.custo = p_cons_I_exp.custo(1:N,:);


  for sub = 1:N_submercado
    %   Folga           1x4                  5904  struct
    Folga(sub).minimo = Folga(sub).minimo(1:N);
    Folga(sub).maximo = Folga(sub).maximo(1:N);
    Folga(sub).custo  = Folga(sub).custo(1:N);

    %   Q               1x4                  5904  struct
    Q(sub).minimo = Q(sub).minimo(1:N);
    Q(sub).maximo = Q(sub).maximo(1:N);
    Q(sub).custo  = Q(sub).custo(1:N);

    %   S               1x4                  5904  struct
    S(sub).maximo = S(sub).maximo(1:N);
    S(sub).minimo = S(sub).minimo(1:N);
    S(sub).custo  = S(sub).custo(1:N);

    %   T
    T(sub).maximo = T(sub).maximo(1:N,:);
    T(sub).minimo = T(sub).minimo(1:N,:);
    T(sub).custo  = T(sub).custo(1:N,:);

    %   V
    V(sub).maximo = V(sub).maximo(1:N);
    V(sub).minimo = V(sub).minimo(1:N);

    %   p_GH_exp
    p_GH_exp(sub).maximo = [p_GH_exp(sub).maximo(:,1:N)]';
    p_GH_exp(sub).minimo = [p_GH_exp(sub).minimo(:,1:N)]';
    p_GH_exp(sub).custo = [p_GH_exp(sub).custo(:,1:N)]';

    %   p_cons_GH_exp
    p_cons_GH_exp(sub).maximo = [p_cons_GH_exp(sub).maximo(:,1:N)]';
    p_cons_GH_exp(sub).minimo = [p_cons_GH_exp(sub).minimo(:,1:N)]';
    p_cons_GH_exp(sub).custo = [p_cons_GH_exp(sub).custo(:,1:N)]';
    
    %   Q_exp
    Q_exp(sub).maximo = [Q_exp(sub).maximo(:,1:N)]';
    Q_exp(sub).minimo = [Q_exp(sub).minimo(:,1:N)]';
    Q_exp(sub).custo = [Q_exp(sub).custo(:,1:N)]';

    %   V_exp
    V_exp(sub).maximo = [V_exp(sub).maximo(:,1:N)]';
    V_exp(sub).minimo = [V_exp(sub).minimo(:,1:N)]';

    %   p_T_exp
    p_T_exp(sub).maximo = p_T_exp(sub).maximo(1:N,:);
    p_T_exp(sub).minimo = p_T_exp(sub).minimo(1:N,:);
    p_T_exp(sub).custo = p_T_exp(sub).custo(1:N,:);

    
    %   p_cons_T_exp
    p_cons_T_exp(sub).maximo = p_cons_T_exp(sub).maximo(1:N,:);
    p_cons_T_exp(sub).minimo = p_cons_T_exp(sub).minimo(1:N,:);
    p_cons_T_exp(sub).custo = p_cons_T_exp(sub).custo(1:N,:);
    
    %   T_exp

    T_exp(sub).maximo = T_exp(sub).maximo(1:N,:);
    T_exp(sub).minimo = T_exp(sub).minimo(1:N,:);
    T_exp(sub).custo = T_exp(sub).custo(1:N,:);
  end




end
