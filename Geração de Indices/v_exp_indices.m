function [ V_Exp_indices ] = v_exp_indices( A_base,V_Exp_indices,num_cen, V_exp )
%V_EXP_INDICES Retorna o indice das vari�veis de expans�o de armazenamento
%   Esta fun��o recebe os indices das vari�veis de expans�o de armazenamento 
%   para o primeiro cen�rio e retorna os indices das vari�veis 
%   de expans�o de armazenamento do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.

% N�mero de vari�veis por submercado, tempo e cen�rio
num_variaveis = size(A_base,2) - V_exp(4).p1;

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    V_Exp_indices = V_Exp_indices(1:size(V_Exp_indices,2) - 4) + ...
        V_Exp_indices(end)*ones(1,size(V_Exp_indices,2) - 4) + num_variaveis; 
else
    aux_V_Exp_temp = v_exp_indices(A_base,V_Exp_indices,num_cen - 1,V_exp);
    V_Exp_indices = V_Exp_indices(1:size(V_Exp_indices,2) - 4) + ...
        aux_V_Exp_temp(end)*ones(1,size(V_Exp_indices,2) - 4) + num_variaveis; 
    
end

end

