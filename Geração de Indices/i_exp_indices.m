function [ I_Exp_indices ] = i_exp_indices( A_base,I_Exp_indices,num_cen, I_exp,N )
%I_EXP_INDICES Retorna o indice das variaveis de expans�o de I
% Esta fun��o recebe os indices das vari�veis de expans�o dos limites 
%   de interc�mbio para o primeiro cen�rio e retorna os indices das vari�veis 
%   de expans�o de limites de interc�mbio do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.
  
% N�mero de vari�veis por submercado, tempo e cen�rio
num_variaveis = kron(ones(1,N-1),(kron(size(A_base,2), ones(1,size(I_exp.p1,2))) - I_exp.p1));

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    I_Exp_indices = I_Exp_indices(1:size(I_Exp_indices,2) - 5) + ...
        kron(ones(1,N-1), I_Exp_indices(size(I_Exp_indices,2) - 4:end)) + num_variaveis; 
else
    aux_I_Exp_temp = i_exp_indices(A_base,I_Exp_indices,num_cen - 1,I_exp,N);
    I_Exp_indices = I_Exp_indices(1:size(I_Exp_indices,2) - 5) + ...
        kron(ones(1,N-1), aux_I_Exp_temp(size(aux_I_Exp_temp,2) - 4:end)) + num_variaveis; 
    
end

end