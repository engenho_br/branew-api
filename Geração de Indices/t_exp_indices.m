function [ T_Exp_indices ] = t_exp_indices( A_base,T_Exp_indices,num_cen, T_exp, N,...
    N_submercado )
%T_EXP_INDICES Retorna o indice das vari�veis de expans�o de t�rmicas
%   Esta fun��o recebe os indices das vari�veis de expans�o de t�rmicas 
%   para o primeiro cen�rio e retorna os indices das vari�veis 
%   de expans�o de t�rmicas do cen�rio num_cen. Isto �
%   feito por meio de uma recurs�o, a qual tem como caso base num_cen = 2.


% Pegando os indices das t�rmicas do primeiro tempo e cen�rio
T_exp_p1 = [];
for i = 1:N_submercado
    T_exp_p1 = [T_exp_p1  T_exp(i).p1];
end

% N�mero de vari�veis por submercado, tempo e cen�rio
num_variaveis = kron(ones(1,N-1),(kron(size(A_base,2), ...
    ones(1,size(T_exp_p1,2))) - T_exp_p1));

% Caso base da recurs�o: N�mero de cen�rios = 2. Todos os outros casos s�o
% reduzidos a n -1 at� chegar a 2 cen�rios
if num_cen==2
    T_Exp_indices = T_Exp_indices(1:size(T_Exp_indices,2) - size(T_exp_p1,2)) + ...
        kron(ones(1,N-1), T_Exp_indices(size(T_Exp_indices,2) -...
        size(T_exp_p1,2) + 1:end)) + num_variaveis; 
else
    aux_T_Exp_temp = t_exp_indices(A_base, T_Exp_indices, num_cen - 1, T_exp, N,...
        N_submercado);
    T_Exp_indices = T_Exp_indices(1:size(T_Exp_indices,2) - size(T_exp_p1,2) ) + ...
        kron(ones(1,N-1), aux_T_Exp_temp(size(aux_T_Exp_temp,2) -...
        size(T_exp_p1,2) + 1:end)) + num_variaveis; 
    
end

end