function [ integer_constraints ] = geracao_indices_variaveis( contC, ...
    num_cenario, N_submercado, p_GH_exp, p_T_exp, p_I_exp, Ab, N, I)
%GERACAO_INDICES_VARIAVEIS Retorna o indices das vari�veis inteiras
%   Esta fun��o pega os indices de cada vari�vel inteira, para todos os
%   cen�rios, tempos e submercados. Ap�s pegar todos os �ndices, ela
%   retorna a lista integer_constraints com estes �ndices.

integer_constraints = [];

for cen = 1:num_cenario
    constraints = contC.*([1:N]-1);
    aux_constraints = kron(constraints,ones(1,N_submercado));
    aux_constraints_I = kron(constraints,ones(1,size(I.linhas,1)));
    
    %----- Primeiro Cen�rio -----
    % GH_EXP
    % Pegando a quantidade de constraints de GH_exp por cen�rio e tempo.
    length_GH_exp = 0;
    for sub = 1:N_submercado
        length_GH_exp = length_GH_exp + length(p_GH_exp(sub).p1);
    end
    
    aux_GH_Exp = kron(ones(1,length(constraints)),[p_GH_exp(:).p1])...
        + kron(constraints,ones(1,length_GH_exp));
    
    
    % I_EXP
    aux_I_Exp = kron(ones(1,N),[p_I_exp.p1]) + aux_constraints_I;
    
    % T_EXP
    
    % Pegando a quantidade de constraints de T_exp por cen�rio e tempo.
    length_T_exp = 0;
    for sub = 1:N_submercado
        length_T_exp = length_T_exp + length(p_T_exp(sub).p1);
    end
    
    aux_T_Exp = kron(ones(1,N),[p_T_exp(:).p1]) + ...
        kron(constraints,ones(1,length_T_exp));
    
    % ----------------------
    
    % ----- DEMAIS CEN�RIOS -------------
    if cen>1
        
        %GH_EXP
        aux_GH_Exp = gh_exp_indices(Ab,aux_GH_Exp,cen,p_GH_exp,N,...
            N_submercado);
        
        %I_EXP
        aux_I_Exp = i_exp_indices(Ab,aux_I_Exp,cen, p_I_exp,N);
        
        %T_EXP
        aux_T_Exp = t_exp_indices(Ab,aux_T_Exp,cen, p_T_exp, N,...
            N_submercado);
        
    end
    
    % Adicionando os indices das vari�veis para o cen�rio cen a lista de
    % indices.
    integer_constraints = [integer_constraints aux_GH_Exp aux_I_Exp aux_T_Exp];
end

integer_constraints = sort(integer_constraints);


end

