function handles=muda_dados(handles)
inst=get(handles.instantes,'Value');
cenario=get(handles.cenario,'Value');
nome_sub=char('N','NE','SE','S');
for ii=1:4
    eval(['handles.A(' num2str(ii) ',' num2str(inst) ',' num2str(cenario) ')=str2num(get(handles.A_' nome_sub(ii,:) ',''String''));']);
    eval(['handles.D(' num2str(ii) ',' num2str(inst) ',' num2str(cenario) ')=str2num(get(handles.D_' nome_sub(ii,:) ',''String''));']);
end
handles=otimizar(handles);
