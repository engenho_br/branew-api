function salvar_cmo_excel( handles )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[nome,caminho]=uiputfile({'*.xls','Arquivo do Excel (*.xls)'},'Salvar Como');
if nome~=0
    nome_sub=char('N','NE','SE','S');
    num_cen=handles.num_cen;
    cmo=handles.cmo;
    h = waitbar(0,'Aguarde...');
    for ii=1:handles.N
        aux_tempo{ii,1}=datestr(handles.tempo(ii,1),'dd/mm/yyyy');
    end
    
    for sub=1:size(nome_sub,1)
        
        aux_A=[];
        aux_D=[];
        
        % Armazenar dados de aflu�ncia e demanda de todos os cen�rios
        % separados por cen�rio e submercado.
        for cenario=1:num_cen
            aux_A=[aux_A num2cell(cmo(cenario).A(:,sub))];
            aux_D=[aux_D num2cell(cmo(cenario).D(:,sub))];
        end
        aux_A=[[{'Instante/Cen�rio'} num2cell([1:size(aux_A,2)])]; [aux_tempo aux_A]];
        aux_D=[[{'Instante/Cen�rio'} num2cell([1:size(aux_D,2)])]; [aux_tempo aux_D]];
        
        % Salvar dados no excel
        xlswrite([caminho nome], aux_A, ['A_' nome_sub(sub,:)]);
        xlswrite([caminho nome], aux_D, ['D_' nome_sub(sub,:)]);
        waitbar(sub/size(nome_sub,1));
    end
    xlswrite([caminho nome],[num2cell([1:size(handles.prob_cen,2)]); num2cell(handles.prob_cen)], 'Probabilidades');
    try close(h); catch end
    dos([caminho nome]);
end

end

