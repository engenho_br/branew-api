function [ edit_N_meses ] = set_N(handles)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Abrir e ler dados do arquivo que cont�m os dados de tempo de in�cio e fim
% do estudo
sheets = {'Dados Gerais'};
diretorio = handles.edit_folder_deck;
try
    
    [a,b] = xlsread([diretorio '\' char(sheets{1}) '.xls']);
    mes_inicial  = a(5);
    ano_inicial  = a(6);
    n_anos = a(3);

    % Determinar o tempo de estudo
    tempo = datevec([datenum(ano_inicial,mes_inicial,1): datenum(ano_inicial+n_anos-1,12,1)]);
    tempo(:,3) = 1;
    tempo = unique(datenum(tempo));

    edit_N_meses = length(tempo);
    

catch
    edit_N_meses = 0;
end
        
% Colocar este tempo no menu Numero de Meses

end

