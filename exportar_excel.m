function exportar_excel( handles )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

for ii=1:4
    eval(['sub(' num2str(ii) ')=handles.check_' num2str(ii) ';']);
end


% Pegar todos os dados e armazenar
informacao_tipo(1) = handles.operacao_radiobutton;
informacao_tipo(2) = handles.expansao_radiobutton;
dados=[];
fl_sub = [];
legenda=[];
nome_sub=char('NO','NE','SE','S');
var=handles.variaveis;
nome_var=handles.variaveis;
cenario=handles.list_cen;
fluxo=handles.list_fluxos;
nome_fluxos=handles.list_fluxos;
custos=handles.list_cmo;
nome_custos=handles.list_cmo;
sub=find(sub);

if informacao_tipo(1)
    informacao_nome = 'Operacao';
else
    informacao_nome = 'Expansao';
end


% Se h� cen�rios, armazene 
if ~isempty(cenario)
    for iii=cenario(1):cenario(end)
        if ~isempty(find(cenario==iii))
            if ~isempty(fluxo)
                 if informacao_nome == 'Operacao'
                    dados=[dados handles.F(iii).F(:,fluxo)];
                    legenda=char(legenda, [char(nome_fluxos{fluxo}) repmat([' - cenario(' num2str(iii) ')'],size(fluxo))]);
                else
                    dados=[dados handles.F_exp(iii).F_exp(:,fluxo)];
                    legenda=char(legenda, [char(nome_fluxos{fluxo}) repmat([' - cenario(' num2str(iii) ')'],size(fluxo))]);
                end
            end
        end
    end
end


% Se h� submercados, adicione-os aos dados para cada cen�rio
if ~isempty(sub)
    %h=msgbox('Aguarde...','modal');
    for ii=sub(1):sub(end)
        % Se h� cen�rios, armazene
        if ~isempty(find(sub==ii)) & ~isempty(cenario)
            for iii=cenario(1):cenario(end)
                if ~isempty(find(cenario==iii))
                    % Se h� t�rmicas, adicione-as aos dados e armazene
                    if ~isempty(termicas{ii})
                        dados=[dados handles.resp(ii).T(:,termicas{ii},iii)];
                        legenda=char(legenda,[handles.T(ii).nome(termicas{ii},:) repmat([' - cenario(' num2str(iii) ')'],size(termicas{ii}))]);
                    end
                    % Se h� vari�veis escolhidas, armazene os dados dessas
                    % vari�veis
                    if ~isempty(var)
                        for iiii=var(1):var(end)
                            if informacao_nome == 'Operacao'
                                if ~isempty(find(var==iiii)) & (iiii<4 | iiii==6)
                                    eval(['dados=[dados ' 'handles.resp(ii).' char(nome_var(iiii)) '(:,' num2str(iii) ')];']);
                                    legenda=char(legenda,[char(nome_var(iiii)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                                elseif ~isempty(find(var==iiii)) & iiii>=4 & iiii~=6
                                    if char(nome_var(iiii)) == 'D'
                                        eval(['dados=[dados ' 'handles.' char(nome_var(iiii)) '(' num2str(ii) ',:'  ')''];']);
                                    else
                                        eval(['dados=[dados ' 'handles.' char(nome_var(iiii)) '(' num2str(ii) ',:,' num2str(iii) ')''];']);
                                    end
                                    legenda=char(legenda,[char(nome_var(iiii)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                                end
                            else
                                if ~isempty(find(var==iiii)) & iiii == 3
                                    eval(['dados=[dados ' 'handles.resp(ii).p_T_exp(:,' num2str(iii) ')];'])
                                else
                                    eval(['dados=[dados ' 'handles.resp(ii).' char(nome_var(iiii,:)) '(:,' num2str(iii) ')];'])
                                end
                                
                                legenda=char(legenda,[char(nome_var(iiii,:)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                            end
                        end
                    end
                end
                %Se h� custos de t�rmica, armazene os custos.
                if ~isempty(custos)
                    if ~isempty(find(custos==1))
                        dados=[dados handles.cmo(iii).A(:,ii)];
                        legenda=char(legenda, [char(nome_custos{1}) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                    end
                    if ~isempty(find(custos==2))
                        dados=[dados handles.cmo(iii).D(:,ii)];
                        legenda=char(legenda, [char(nome_custos{2}) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                    end
                end
            end
        end
        
        
        fl_sub = [fl_sub ones(1,size(dados,2)-size(fl_sub,2))*ii];
    end
end

% Verifica Normaliza��o
if get(handles.normalizado,'Value')
    for ii=1:size(dados,2)
        if sum(dados(:,ii))~=0
            dados(:,ii)=normaliza(dados(:,ii));
        end
    end
end

% Tirando a legenda vazia
if size(legenda,1) > 0
    legenda(1,:) = [];
end

%Se h� dados, coloque-os numa planilha do excel e salve no diret�rio escolhido pelo usu�rio.
if ~isempty(dados)
    [filename, pathname] = uiputfile('saida.xls', 'Exportar para...');
    if filename(1)~=0
        
        % Verifique se um arquivo com esse nome existe, se existir, delete
        if exist(filename, 'file') == 2
            delete(filename);
        end
     
        nome_sub = [{'Interc�mbios'}; cellstr(nome_sub)];
        
        
        for iii = 0:4
            % Armazene os fluxos de cada submercado.
            aux_dados = dados(:,fl_sub==iii);
            aux_legenda = legenda(fl_sub==iii,:);
            
            % Se h� dados, salve estes dados no excel
            if ~isempty(aux_dados)
                for ii=1:size(aux_legenda,1)
                    texto{ii}=aux_legenda(ii,:);
                end
                
                tempo=handles.tempo;
                t=cellstr(datestr(tempo,'dd/mm/yyyy'));
                d=num2cell(aux_dados);
                saida=[{'Data'} texto; t d];
                xlswrite([pathname filename],saida,nome_sub{iii+1});
            end
        end
    end
else
    msgbox('N?o h� dados escolhidos para a gera�?o do arquivo excel.','Aviso!');
end
end

