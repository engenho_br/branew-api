function plotador(handles)
for ii=1:4
    eval(['sub(' num2str(ii) ')=handles.check_' num2str(ii) ';']);
end
% 
informacao_tipo(1) = handles.operacao_radiobutton;
informacao_tipo(2) = handles.expansao_radiobutton;
dados=[];
legenda=[];
nome_sub=char('NO','NE','SE','S');
var=handles.variaveis;
nome_var=handles.variaveis;
cenario=handles.list_cen;

fluxo=handles.list_fluxos;
nome_fluxos=handles.list_fluxos;
custos=handles.list_cmo;
nome_custos=handles.list_cmo;
sub=find(sub);

% Determinar se procura as informa��es de expans�o ou de opera��o
if informacao_tipo(1)
    informacao_nome = 'Operacao';
else
    informacao_nome = 'Expansao';
end
% Se houver cen�rio, plote o gr�fico cen�rio por cen�rio
if ~isempty(cenario)
    for iii=cenario(1):cenario(end)
        
        %Verifique se o cen�rio iii existe
        if ~isempty(find(cenario==iii))

            %Verifique se h� fluxo
            if ~isempty(fluxo)
                if informacao_nome == 'Operacao'
                    dados=[dados handles.F(iii).F(:,fluxo)];
                    legenda=char(legenda, [char(nome_fluxos{fluxo}) repmat([' - cenario(' num2str(iii) ')'],size(fluxo))]);
                else
                    dados=[dados handles.F_exp(iii).F_exp(:,fluxo)];
                    legenda=char(legenda, [char(nome_fluxos{fluxo}) repmat([' - cenario(' num2str(iii) ')'],size(fluxo))]);
                end
            end
        end
    end
end


% Se houver submercado, plote o gr�fico cen�rio por cen�rio para um
% submercado espec�fico
if ~isempty(sub)

    for ii=sub(1):sub(end)
        %Verifique se h� cen�rios
        if ~isempty(find(sub==ii)) & ~isempty(cenario)
            for iii=cenario(1):cenario(end)
                %Verifique se o cen�rio iii existe
                if ~isempty(find(cenario==iii))
                    
                    %Verifique se existem t�rmicas para o submercado ii
                    if ~isempty(termicas{ii})
                        % Dados das t�rmicas do submercado ii para o
                        % cen�rio
                        
                    end
                    
                    % Verifique se h� vari�veis escolhidas para seus valores serem plotados no gr�fico                    
                    if ~isempty(var)
                        for iiii=var(1):var(end)
                            
                            if informacao_nome == 'Operacao'
                                if ~isempty(find(var==iiii)) & (iiii<4 | iiii==6)
                                    eval(['dados=[dados ' 'handles.resp(ii).' char(nome_var(iiii)) '(:,' num2str(iii) ')];']);
                                    legenda=char(legenda,[char(nome_var(iiii)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                                elseif ~isempty(find(var==iiii)) & iiii>=4 & iiii~=6
                                    if char(nome_var(iiii)) == 'D'
                                        eval(['dados=[dados ' 'handles.' char(nome_var(iiii)) '(' num2str(ii) ',:'  ')''];']);
                                    else
                                        eval(['dados=[dados ' 'handles.' char(nome_var(iiii)) '(' num2str(ii) ',:,' num2str(iii) ')''];']);
                                    end
                                    legenda=char(legenda,[char(nome_var(iiii)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                                end
                            else
                                if ~isempty(find(var==iiii)) & iiii == 3
                                    eval(['dados=[dados ' 'handles.resp(ii).p_T_exp(:,' num2str(iii) ')];'])
                                else
                                    eval(['dados=[dados ' 'handles.resp(ii).' char(nome_var(iiii,:)) '(:,' num2str(iii) ')];'])
                                end
                                
                                legenda=char(legenda,[char(nome_var(iiii,:)) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                            end
                        end
                    end
                end
                % Se houver custos, adicione os dados de custos aos dados
                % que ir�o ser plotados
                if ~isempty(custos)
                    if ~isempty(find(custos==1))
                        dados=[dados handles.cmo(iii).A(:,ii)];
                        legenda=char(legenda, [char(nome_custos{1}) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                    end
                    if ~isempty(find(custos==2))
                        dados=[dados handles.cmo(iii).D(:,ii)];
                        legenda=char(legenda, [char(nome_custos{2}) ' - ' nome_sub(ii,:) ' - cenario(' num2str(iii) ')']);
                    end
                end
            end
        end
        if ~isempty(custos) & any(sub==ii)
            %%% CMO M�dio da Aflu�ncia
            if any(custos==3)
                aux = [];
                for CEN = 1:handles.num_cen
                    aux = [aux handles.cmo(CEN).A(:,ii)];
                end
                dados = [dados mean(aux,2)];
                legenda = char(legenda, ['CMO A M�dio - ' nome_sub(ii,:)]);
            end
            
            %%% CMO M�dio da Demanda
            if any(custos==4)
                aux = [];
                for CEN = 1:handles.num_cen
                    aux = [aux handles.cmo(CEN).D(:,ii)];
                end
                dados = [dados mean(aux,2)];
                legenda = char(legenda, ['CMO D M�dio - ' nome_sub(ii,:)]);
            end
        end
    end
end

% Verifica Normaliza��o
if get(handles.normalizado,'Value')
    for ii=1:size(dados,2)
        if sum(dados(:,ii))~=0
            dados(:,ii)=normaliza(dados(:,ii));
        end
    end
end
end