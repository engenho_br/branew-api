function handles=verifica_energias_deck(handles)

    folder_deck = get(handles.edit_folder_deck,'String');
    set(handles.ok,'Enable','off')

    %%% Energia F
    if ~exist([folder_deck '\energiaf.dat'],'file')
        set(handles.rd_energiaf,'Enable','off','Value',0);
        set(handles.rd_energiab,'Value',1);
    else
        set(handles.rd_energiaf,'Enable','on');
        set(handles.ok,'Enable','on');
    end

    %%% Energia B
    if ~exist([folder_deck '\energiab.dat'],'file')
        set(handles.rd_energiab,'Enable','off','Value',0);
        set(handles.rd_eafpast,'Value',1);
    else
        set(handles.rd_energiab,'Enable','on');
        set(handles.ok,'Enable','on');
    end

    %%% C�lculo das energias sint�ticas
    if ~exist([folder_deck '\parp.dat'],'file') || ~exist([folder_deck '\pmo.dat'],'file')
        set(handles.rd_eafpast,'Enable','off','Value',0);
        set(handles.pop_eafpast,'Enable','off');
        if strcmpi(get(handles.rd_energiaf,'Enable'),'off')
            set(handles.rd_historico,'Value',1);
        end
    else
        set(handles.rd_eafpast,'Enable','on');
        set(handles.pop_eafpast,'Enable','on');
        set(handles.ok,'Enable','on');
    end

    %%% C�lculo das energias sint�ticas
    arq_historico = {
        'Energia Afluente Norte'        % 29
        'Energia Afluente Nordeste'     % 20
        'Energia Afluente Sudeste'      % 21
        'Energia Afluente Sul'          % 22
        'Energia Fio Norte'             % 23
        'Energia Fio Nordeste'          % 24
        'Energia Fio Sudeste'           % 25
        'Energia Fio Sul'               % 26
        };

    existe = 1;
    for ii = 1:length(arq_historico)
        if ~exist([folder_deck '\' arq_historico{ii} '.xls'])
            existe = 0;
        end
    end

    if existe == 0
        set(handles.rd_historico,'Enable','off','Value',0);
    else
        set(handles.rd_historico,'Enable','on');
        set(handles.ok,'Enable','on');
end